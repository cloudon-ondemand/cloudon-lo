# -*- Mode: makefile-gmake; tab-width: 4; indent-tabs-mode: t -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

ifeq ($(OS), IOS)
tmp_gb_LinkTarget_CFLAGS := $(gb_LinkTarget_CFLAGS)
tmp_gb_LinkTarget_CXXFLAGS := $(gb_LinkTarget_CXXFLAGS)
tmp_gb_LinkTarget_OBJCXXFLAGS := $(gb_LinkTarget_OBJCXXFLAGS)
tmp_gb_LinkTarget_OBJCFLAGS := $(gb_LinkTarget_OBJCFLAGS)
gb_LinkTarget_CFLAGS += -g
gb_LinkTarget_CXXFLAGS += -g
gb_LinkTarget_OBJCXXFLAGS += -g
gb_LinkTarget_OBJCFLAGS += -g
endif

$(eval $(call gb_Module_Module,bridges))

$(eval $(call gb_Module_add_targets,bridges,\
	Library_cpp_uno \
	$(if $(ENABLE_JAVA),\
		Jar_java_uno \
		Library_java_uno \
		$(if $(filter MACOSX,$(OS)),Package_jnilib_java_uno) \
	) \
	$(if $(filter ARM,$(CPUNAME)),\
		$(if $(filter IOS,$(OS)),\
			CustomTarget_gcc3_ios_arm) \
		$(if $(filter ANDROID LINUX,$(OS)),\
			CustomTarget_gcc3_linux_arm) \
	) \
))

ifeq (,$(filter build,$(gb_Module_SKIPTARGETS)))
ifeq ($(strip $(bridges_SELECTED_BRIDGE)),)
$(call gb_Output_error,no bridge selected for build: bailing out)
else ifneq ($(words $(bridges_SELECTED_BRIDGE)),1)
$(call gb_Output_error,multiple bridges selected for build: $(bridges_SELECTED_BRIDGE))
endif
endif

ifeq ($(OS), IOS)
gb_LinkTarget_CFLAGS := $(tmp_gb_LinkTarget_CFLAGS)
gb_LinkTarget_CXXFLAGS := $(tmp_gb_LinkTarget_CXXFLAGS)
gb_LinkTarget_OBJCXXFLAGS := $(tmp_gb_LinkTarget_OBJCXXFLAGS)
gb_LinkTarget_OBJCFLAGS := $(tmp_gb_LinkTarget_OBJCFLAGS)
endif

# vim: set noet sw=4 ts=4:
