#!/bin/bash

cwd=$(pwd)

if [ $# -eq 1 ]
then
    file="$1"
else
    echo "resetFileTimeStamp <file>"
    exit 7
fi

touch -t 0101010101 $file

