/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * Copyright 2013 LibreOffice contributors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <touch/touch-enums.h>

#ifndef INCLUDED_TOUCH_TOUCH_CONTENT_H
#define INCLUDED_TOUCH_TOUCH_CONTENT_H

#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif
#endif

// USAGE:
// 1. to add a hook, add
// 1.1. to the include area:
/// #if !HAVE_FEATURE_DESKTOP
/// #include "touch/touch-content.h"
/// #endif
// 1.2. the actual hook:
/// #if !HAVE_FEATURE_DESKTOP
/// touch_ui_did_encounter_feature(TouchUiFeatureEnum_<TYPE>, (int)<ENUM_VALUE>);
/// #endif
// 2. to add a differnt enum:
// 2.1. for an enum which already exists in libreoffice add a new enum value to TouchUiFeatureEnum
/// format: TouchUiFeatureEnum_<THE_ENUM_NAME>
// 2.2. for an enum which does not exist yet in libreoffice, or to denote a special case
//      add a new value to TouchUIFeatureCustom, and add a comment what it stands for
/// format: TouchUIFeatureCustom_<THE_NEW_ENUM_NAME>, // changes the bla of the ble
//

// NOTE:
// using MLOEnum, to autogenerate enum to string conversion

// ------------------------------
// TUFE = Touch UI Feature Enum
// ------------------------------

MLOEnum(TouchUiFeatureType,
        TUFE_DOCX_PresToken,
        TUFE_DOCX_PresNonToken,
        TUFE_DOCX_RndrToken,
        TUFE_DOCX_RndrNonToken,
        //TouchUiFeatureEnum_LAST
)

MLOEnum(TUFE_DOCX_PresToken_Enums,

        // [01]  Text
        TUFE_DOCX_PresToken_Text_Kerning,                                                   // Text - Kerning

        // [02]  Paragraph
        TUFE_DOCX_PresToken_Paragraph_Border_Between,                                       // Paragraph - Border - Between
        TUFE_DOCX_PresToken_Paragraph_Border_Bar,                                           // Paragraph - Border - Bar
//      TUFE_DOCX_PresToken_Paragraph_Shading,                                              // Paragraph - Shading
        TUFE_DOCX_PresToken_Paragraph_DropCap,                                              // Paragraph - Drop-Cap
        TUFE_DOCX_PresToken_Paragraph_PageBreakBefore,                                      // Paragraph - Page-Break Before
        TUFE_DOCX_PresToken_Paragraph_BarTab,                                               // Paragraph - Bar Tab
        TUFE_DOCX_PresToken_Paragraph_DontHyphenate,                                        // Paragraph - Don't Hyphenate
        TUFE_DOCX_PresToken_Paragraph_ContextualSpacingTurnedOff,                           // Paragraph - Contextual Spacing Turned Off
                                                                
        // [03]  List
        TUFE_DOCX_PresToken_List_BulletColor,                                               // List - Bullet Color
        TUFE_DOCX_PresToken_List_TextEffect_Outline,                                        // List - Text Effect [Outline]
        TUFE_DOCX_PresToken_List_TextEffect_Fill,                                           // List - Text Effect [Fill]
        TUFE_DOCX_PresToken_List_TextEffect_Shadow,                                         // List - Text Effect [Shadow]
        TUFE_DOCX_PresToken_List_TextEffect_Glow,                                           // List - Text Effect [Glow]
        TUFE_DOCX_PresToken_List_TextEffect_Reflection,                                     // List - Text Effect [Reflection]
        TUFE_DOCX_PresToken_List_TextEffect_3DFormat,                                       // List - Text Effect [3D-Format]
        TUFE_DOCX_PresToken_List_OpenType_Ligatures,                                        // List - OpenType Feature [Ligatures]
        TUFE_DOCX_PresToken_List_OpenType_NumberSpacing,                                    // List - OpenType Feature [Number Spacing]
        TUFE_DOCX_PresToken_List_OpenType_NumberForms,                                      // List - OpenType Feature [Number Forms]
        TUFE_DOCX_PresToken_List_OpenType_StylisticSets,                                    // List - OpenType Feature [Stylistic Sets]
        TUFE_DOCX_PresToken_List_OpenType_ContexualAlternates,                              // List - OpenType Feature [Contexual Alternates]
        TUFE_DOCX_PresToken_List_NumberedStyles_CardinalText,                               // List - Numbered Styles - Cardinal Text [One, Two, ...]
        TUFE_DOCX_PresToken_List_NumberedStyles_Ordinal,                                    // List - Numbered Styles - Ordinal [1st, 2nd, ...]
        TUFE_DOCX_PresToken_List_NumberedStyles_OrdinalText,                                // List - Numbered Styles - Ordinal Text [First, Second, ...]
        TUFE_DOCX_PresToken_List_NumberedStyles_ChicagoManualOfStyle,                       // List - Numbered Styles - Chicago Manual of Style
        TUFE_DOCX_PresToken_List_LegalStyleNumbering,                                       // List - Legal Style Numbering
        TUFE_DOCX_PresToken_List_LevelPictureBulletId,                                      // List - Level Picture Bullet Id

        // [04]  Table
        TUFE_DOCX_PresToken_Table_DeletedCell,                                              // Table - Deleted Cell
//      TUFE_DOCX_PresToken_Table_CellShadingValue,                                         // Table - Cell Shading Value
//      TUFE_DOCX_PresToken_Table_ConditionalFormatting_TblLook,                            // Table - Conditional Formatting
//      TUFE_DOCX_PresToken_Table_ConditionalFormatting_CnfStyle,                           // Table - CNF Style
        TUFE_DOCX_PresToken_Table_AlternativeText_Title,                                    // Table - Alternative Text - Title
        TUFE_DOCX_PresToken_Table_AlternativeText_Description,                              // Table - Alternative Text - Description
        TUFE_DOCX_PresToken_Table_TableRowProperties_TableCellSpacing,                      // Table - Table Row Properties - Table Cell Spacing
        TUFE_DOCX_PresToken_Table_TableProperties_TableCellSpacing,                         // Table - Table Properties - Table Cell Spacing
        TUFE_DOCX_PresToken_Table_TableProperties_Alignment,                                // Table - Table Properties - Alignment
        
        // [05]  Chart
        TUFE_DOCX_PresToken_Chart_TypePieOfPie,                                             // Chart - Pie of Pie
        TUFE_DOCX_PresToken_Chart_TypeBarOfPie,                                             // Chart - Bar of Pie
        TUFE_DOCX_PresToken_Chart_TypeRadar,                                                // Chart - Radar
        TUFE_DOCX_PresToken_Chart_TypeSurface2D,                                            // Chart - Surface 2D
        TUFE_DOCX_PresToken_Chart_TypeSurface3D,                                            // Chart - Surface 3D
        TUFE_DOCX_PresToken_Chart_TypeArea2D,                                               // Chart - Area 2D
        TUFE_DOCX_PresToken_Chart_TypeScatter,                                              // Chart - Scatter
        TUFE_DOCX_PresToken_Chart_TypeScatter_LineMarker,                                   // Chart - Scatter - Line Marker
        TUFE_DOCX_PresToken_Chart_TypeScatter_SmoothMarker,                                 // Chart - Scatter - Smooth Marker
        TUFE_DOCX_PresToken_Chart_TypeStock,                                                // Chart - Stock
        TUFE_DOCX_PresToken_Chart_TypeLine,                                                 // Chart - Line
        TUFE_DOCX_PresToken_Chart_DropLines,                                                // Chart - Drop-Lines
        TUFE_DOCX_PresToken_Chart_HiLowLines,                                               // Chart - Hi-Low-Lines
        TUFE_DOCX_PresToken_Chart_LegendOverlayTrue,                                        // Chart - Legend Overlay True
        TUFE_DOCX_PresToken_Chart_DateAxis,                                                 // Chart - Date Axis

        // [06]  Text-Box
        TUFE_DOCX_PresToken_TextBox_TextDirection_EaVert,                                   // Text-Box - East Asian Vertical
        TUFE_DOCX_PresToken_TextBox_TextDirection_Vert,                                     // Text-Box - Vertical
        TUFE_DOCX_PresToken_TextBox_TextDirection_HorzNormEA,                               // Text-Box - Horizontal (Normal East Asian Flow)
        TUFE_DOCX_PresToken_TextBox_TextDirection_WordArtVert,                              // Text-Box - WordArt Vertical
        TUFE_DOCX_PresToken_TextBox_TextDirection_MongolianVert,                            // Text-Box - Mongolian Vertical
        TUFE_DOCX_PresToken_TextBox_TextDirection_WordArtVertRtl,                           // Text-Box - Vertical WordArt Right to Left
        TUFE_DOCX_PresToken_TextBox_LinkedToOtherTextBox,                                   // Text-Box - Linked to Other Text-Box
        TUFE_DOCX_PresToken_TextBox_WrapTextInShape_None,                                   // Text-Box - Wrap Text In Shape - None
        
        // [07]  Picture
        TUFE_DOCX_PresToken_Picture_FlipHorizontal,                                         // Picture - Flip Horizontal
        TUFE_DOCX_PresToken_Picture_FlipVertical,                                           // Picture - Flip Vertical
        TUFE_DOCX_PresToken_Picture_Fill_Solid,                                             // Picture - Fill - Solid
        TUFE_DOCX_PresToken_Picture_Fill_Gradient,                                          // Picture - Fill - Gradient
        TUFE_DOCX_PresToken_Picture_Fill_Texture,                                           // Picture - Fill - Texture
        TUFE_DOCX_PresToken_Picture_Fill_Pattern,                                           // Picture - Fill - Pattern
        TUFE_DOCX_PresToken_Picture_Outline_Solid,                                          // Picture - Outline - Solid
        TUFE_DOCX_PresToken_Picture_Outline_Gradient,                                       // Picture - Outline - Gradient
        
        // Generic Shape (Text-Box / Shape)
        TUFE_DOCX_PresToken_GenericShape_FlipHorizontal_InfoOnly,                           // Generic Shape - Flip Horizontal
        TUFE_DOCX_PresToken_GenericShape_FlipVertical_InfoOnly,                             // Generic Shape - Flip Vertical
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_NotRotateWithShape,                  // Generic Shape - Fill - Gradient - Not Rotate With Shape
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_MoreThan2Colors,                     // Generic Shape - Fill - Gradient - More Than 2 Colors
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_Position,                            // Generic Shape - Fill - Gradient - Position
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_FillToRect,                          // Generic Shape - Fill - Gradient - Fill To Rect
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_TileRect,                            // Generic Shape - Fill - Gradient - Tile Rect
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_TypePath,                            // Generic Shape - Fill - Gradient - Type Path
        TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_ColorManipulation,                   // Generic Shape - Fill - Gradient - Color Manipulation
        TUFE_DOCX_PresToken_GenericShape_Fill_Pattern,                                      // Generic Shape - Fill - Pattern
        TUFE_DOCX_PresToken_GenericShape_Outline_Gradient,                                  // Generic Shape - Outline - Gradient
        TUFE_DOCX_PresToken_GenericShape_TextEffect_Transformation,                         // Generic Shape - Text Effects - Transform
        TUFE_DOCX_PresToken_GenericShape_FontRefSchemeColor,                                // Generic Shape - Font Ref Scheme Color
        
        // Generic Drawing (Text-Box / Shape / Diagram)
        TUFE_DOCX_PresToken_GenericDrawing_WrapType_Tight_InfoOnly,                         // Generic Drawing - Wrap Type - Tight
        TUFE_DOCX_PresToken_GenericDrawing_WrapType_Through_InfoOnly,                       // Generic Drawing - Wrap Type - Through
        TUFE_DOCX_PresToken_GenericDrawing_WrapPolygon_Tight_InfoOnly,                      // Generic Drawing - Wrap Polygon - Tight
        TUFE_DOCX_PresToken_GenericDrawing_WrapPolygon_Through_InfoOnly,                    // Generic Drawing - Wrap Polygon - Through
        TUFE_DOCX_PresToken_GenericDrawing_Hyperlink,                                       // Generic Drawing - Hyperlink
        TUFE_DOCX_PresToken_GenericDrawing_AnchorLocked,                                    // Generic Drawing - Anchor Locked
        
        // [09]  Diagram
        TUFE_DOCX_PresToken_Diagram_SmartArt,                                               // Diagram - Smart-Art

        // [10]  Page-Setup
        TUFE_DOCX_PresToken_PageSetup_Gutter,                                               // Page-Setup - Gutter
        TUFE_DOCX_PresToken_PageSetup_2PagesPerSheet,                                       // Page-Setup - 2 Pages Per Sheet
        TUFE_DOCX_PresToken_PageSetup_BookFold,                                             // Page-Setup - Book Fold
        TUFE_DOCX_PresToken_PageSetup_ReverseBookFold,                                      // Page-Setup - Reverse Book Fold
//      TUFE_DOCX_PresToken_PageSetup_MirrorMargins,                                        // Page-Setup - Mirror Margins
        TUFE_DOCX_PresToken_PageSetup_AutoHyphenation,                                      // Page-Setup - Auto Hyphenation
        TUFE_DOCX_PresToken_PageSetup_DoNotHyphenateCaps,                                   // Page-Setup - Do Not Hyphenate Caps
        TUFE_DOCX_PresToken_PageSetup_ConsecutiveHyphenLimit,                               // Page-Setup - Consecutive Hyphen Limit
        TUFE_DOCX_PresToken_PageSetup_HyphenationZone,                                      // Page-Setup - Hyphenation Zone
        TUFE_DOCX_PresToken_PageSetup_RestartLineNumbersEachSection,                        // Page-Setup - Restart Line Numbers Each Section
        TUFE_DOCX_PresToken_PageSetup_OnlyAllowEditingOfFormFieldsInSection_Off,            // Page-Setup - Only Allow Editing Of Form Fields In Section - Off
        TUFE_DOCX_PresToken_PageSetup_OnlyAllowEditingOfFormFieldsInSection_On,             // Page-Setup - Only Allow Editing Of Form Fields In Section - On
        TUFE_DOCX_PresToken_PageSetup_SectionBreak_Continuous,                              // Page-Setup - Section Break - Continuous
        TUFE_DOCX_PresToken_PageSetup_Columns_EqualWidthNotSet_ForMoreThanOne,              // Page-Setup - Columns - Equal Width Not Set [For More Than One Column]
        TUFE_DOCX_PresToken_PageSetup_Columns_MoreThan2Columns,                             // Page-Setup - Columns - More than 2 Columns

        // [11]  Page Background
        TUFE_DOCX_PresToken_PageBackground_PageColor_Solid,                                 // Page Background - Page Color - Solid
        TUFE_DOCX_PresToken_PageBackground_PageColor_LinearGradient,                        // Page Background - Page Color - Linear Gradient
        TUFE_DOCX_PresToken_PageBackground_PageColor_RadialGradient,                        // Page Background - Page Color - Radial Gradient
        TUFE_DOCX_PresToken_PageBackground_PageColor_TiledImage,                            // Page Background - Page Color - Tiled Image
        TUFE_DOCX_PresToken_PageBackground_PageColor_ImagePattern,                          // Page Background - Page Color - Image Pattern
        TUFE_DOCX_PresToken_PageBackground_PageColor_StretchImageToFit,                     // Page Background - Page Color - Stretch Image To Fit
        TUFE_DOCX_PresToken_PageBackground_PictureWatermark,                                // Page Background - Picture Watermark
        TUFE_DOCX_PresToken_PageBackground_TextWatermark,                                   // Page Background - Text Watermark
        
        // Border
        TUFE_DOCX_PresToken_BorderStyles_DotDashed,                                         // Border Styles - Dot Dashed
        TUFE_DOCX_PresToken_BorderStyles_DotDotDashed,                                      // Border Styles - Dot Dot Dashed
        TUFE_DOCX_PresToken_BorderStyles_Triple,                                            // Border Styles - Triple
        TUFE_DOCX_PresToken_BorderStyles_ThinThickThinMediumGap,                            // Border Styles - Thin Thick Thin Medium Gap
        TUFE_DOCX_PresToken_BorderStyles_Wave,                                              // Border Styles - Wave
        TUFE_DOCX_PresToken_BorderStyles_DoubleWave,                                        // Border Styles - Double Wave
        TUFE_DOCX_PresToken_BorderStyles_DashDotStroked,                                    // Border Styles - Dash Dot Stroked
        TUFE_DOCX_PresToken_BorderStyles_Outset,                                            // Border Styles - Outset
        TUFE_DOCX_PresToken_BorderStyles_Inset,                                             // Border Styles - Inset
        
        // [12]  Header-Footer
        TUFE_DOCX_PresToken_HeaderFooter_PositionalTab,                                     // Header/Footer - Positional Tab
//      TUFE_DOCX_PresToken_HeaderFooter_ShapeInHeaderFooter,                               // Header/Footer - Shape In Header/Footer
//      TUFE_DOCX_PresToken_HeaderFooter_ChartInHeaderFooter,                               // Header/Footer - Chart In Header/Footer
        TUFE_DOCX_PresToken_HeaderFooter_SdtContentInHeaderFooter_InfoOnly,                 // Header/Footer - SDT Content In Header/Footer
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_LowerRoman,                             // Header/Footer - Page Number - Lower Roman
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_UpperRoman,                             // Header/Footer - Page Number - Upper Roman
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_LowerAlphabetical,                      // Header/Footer - Page Number - Lower Alphabetical
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_UpperAlphabetical,                      // Header/Footer - Page Number - Upper Alphabetical
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_HyphenedDecimal,                        // Header/Footer - Page Number - Hyphened Decimal
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_ChapterStyle,                           // Header/Footer - Page Number - Chapter Style
        TUFE_DOCX_PresToken_HeaderFooter_PageNumber_ChapterSeparator,                       // Header/Footer - Page Number - Chapter Separator
        
        // [13]  Object
        TUFE_DOCX_PresToken_Object_OleObject,                                               // Object - OLE Object
        TUFE_DOCX_PresToken_Object_Type_Link,                                               // Object - Type Link
        TUFE_DOCX_PresToken_Object_Excel_97_2003_Worksheet,                                 // Object - Excel 97-2003 Worksheet
        TUFE_DOCX_PresToken_Object_Excel_2010_Worksheet,                                    // Object - Excel 2010 Worksheet
        TUFE_DOCX_PresToken_Object_Excel_97_2003_Chart,                                     // Object - Excel 97-2003 Chart
        TUFE_DOCX_PresToken_Object_PowerPoint_97_2003_Presentation,                         // Object - PowerPoint 97-2003 Presentation
        TUFE_DOCX_PresToken_Object_PowerPoint_2010_Presentation,                            // Object - PowerPoint 2010 Presentation
        TUFE_DOCX_PresToken_Object_PowerPoint_97_2003_Slide,                                // Object - PowerPoint 97-2003 Slide
        TUFE_DOCX_PresToken_Object_PowerPoint_2010_Slide,                                   // Object - PowerPoint 2010 Slide
        TUFE_DOCX_PresToken_Object_Word_97_2003_Document,                                   // Object - Word 97-2003 Document
        TUFE_DOCX_PresToken_Object_Word_2010_Document,                                      // Object - Word 2010 Document
        TUFE_DOCX_PresToken_Object_Word_OpenDocumentText_12,                                // Object - Word OpenDocumentText 12
        TUFE_DOCX_PresToken_Object_SoundRec,                                                // Object - Sound Rec
        TUFE_DOCX_PresToken_Object_PBrush,                                                  // Object - PBrush
        TUFE_DOCX_PresToken_Object_LinkToPackage,                                           // Object - Link to Package
        TUFE_DOCX_PresToken_Object_DrawAsIcon,                                              // Object - Draw As Icon

        // [14]  Forms
        TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_Type,                                  // Forms - Legacy Form - Text-Box - Type
        TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_DefaultText,                           // Forms - Legacy Form - Text-Box - Default Text
        TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_MaxLength,                             // Forms - Legacy Form - Text-Box - Max Length
        TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_TextFormat,                            // Forms - Legacy Form - Text-Box - Text Format
        TUFE_DOCX_PresToken_Forms_LegacyForm_Checkbox_Size,                                 // Forms - Legacy Form - Check-Box - Size
        TUFE_DOCX_PresToken_Forms_LegacyForm_Checkbox_DefaultValue,                         // Forms - Legacy Form - Check-Box - Default Value
        TUFE_DOCX_PresToken_Forms_LegacyForm_Frame_TextFrameProperties_InBody,              // Forms - Legacy Form - Frame - Text Frame Properties [In Body]
        TUFE_DOCX_PresToken_Forms_LegacyForm_Frame_TextFrameProperties_InHeaderFooter,      // Forms - Legacy Form - Frame - Text Frame Properties [In Header/Footer]
        TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_FillInDisabled,                   // Forms - Legacy Form - Field Setting - Fill-In Disabled
        TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_CalcOnExit,                       // Forms - Legacy Form - Field Setting - Calc on Exit
        TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_HelpText,                         // Forms - Legacy Form - Field Setting - Help Text
        TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_StatusText,                       // Forms - Legacy Form - Field Setting - Status Text
        TUFE_DOCX_PresToken_Forms_LegacyForm_DoNotShadeFormData,                            // Forms - Legacy Form - Do Not Shade Form Data

        // [15]  References
        TUFE_DOCX_PresToken_References_Citation_Flag_L,                                     // References - Citation - Flag L
        TUFE_DOCX_PresToken_References_Citation_Flag_V,                                     // References - Citation - Flag V
        TUFE_DOCX_PresToken_References_Citation_Flag_F,                                     // References - Citation - Flag F
        TUFE_DOCX_PresToken_References_Citation_Flag_S,                                     // References - Citation - Flag S
        TUFE_DOCX_PresToken_References_Citation_Flag_M,                                     // References - Citation - Flag M
        TUFE_DOCX_PresToken_References_SectionWideFootnoteProps_BeneathText,                // References - Section-Wide Footnote Properties - Beneath Text
        TUFE_DOCX_PresToken_References_SectionWideFootnoteProps_EndOfSection,               // References - Section-Wide Footnote Properties - End of Section
        TUFE_DOCX_PresToken_References_SectionWideEndnoteProps_EndOfSection,                // References - Section-Wide Endnote Properties - End of Section
        TUFE_DOCX_PresToken_References_DocumentWideFootnoteProps_BeneathText,               // References - Document-Wide Footnote Properties - Beneath Text
        TUFE_DOCX_PresToken_References_DocumentWideFootnoteProps_EndOfSection,              // References - Document-Wide Footnote Properties - End of Section
        TUFE_DOCX_PresToken_References_DocumentWideEndnoteProps_EndOfSection,               // References - Document-Wide Endnote Properties - End of Section
        TUFE_DOCX_PresToken_References_FootnoteProps_NumberedStyles_ChicagoManualOfStyle,   // References - Footnote Properties - Numbered Styles - Chicago Manual of Style
        TUFE_DOCX_PresToken_References_EndnoteProps_NumberedStyles_ChicagoManualOfStyle,    // References - Endnote Properties - Numbered Styles - Chicago Manual of Style
        TUFE_DOCX_PresToken_References_SectionWideFootnoteEndnoteProps_RestartEachPage,     // References - Section-Wide Footnote / Endnote Properties - Restart Each Page
        TUFE_DOCX_PresToken_References_SectionWideFootnoteEndnoteProps_RestartEachSection,  // References - Section-Wide Footnote / Endnote Properties - Restart Each Section
        TUFE_DOCX_PresToken_References_DocumentWideFootnoteEndnoteProps_RestartEachPage,    // References - Document-Wide Footnote / Endnote Properties - Restart Each Page
        TUFE_DOCX_PresToken_References_DocumentWideFootnoteEndnoteProps_RestartEachSection, // References - Document-Wide Footnote / Endnote Properties - Restart Each Section

        // [16]  Link
        TUFE_DOCX_PresToken_Link_Tooltip,                                                   // Header/Footer - Link - Tooltip

        // [17]  Field
        TUFE_DOCX_PresToken_Field_DateAndTime_EditTime,                                     // Field - Date and Time - Edit Time
        TUFE_DOCX_PresToken_Field_DateAndTime_CreateDate,                                   // Field - Date and Time - Create Date
        TUFE_DOCX_PresToken_Field_DocumentProperty_Author,                                  // Field - Document Property - Author
        TUFE_DOCX_PresToken_Field_DocumentProperty_Comments,                                // Field - Document Property - Comments
        TUFE_DOCX_PresToken_Field_DocumentProperty_Filename,                                // Field - Document Property - Filename
        TUFE_DOCX_PresToken_Field_DocumentProperty_Keywords,                                // Field - Document Property - Keywords
        TUFE_DOCX_PresToken_Field_DocumentProperty_LastSavedBy,                             // Field - Document Property - Last Saved By
        TUFE_DOCX_PresToken_Field_DocumentProperty_NumChars,                                // Field - Document Property - NumChars
        TUFE_DOCX_PresToken_Field_DocumentProperty_NumWords,                                // Field - Document Property - NumWords
        TUFE_DOCX_PresToken_Field_DocumentProperty_NumPages,                                // Field - Document Property - NumPages
        TUFE_DOCX_PresToken_Field_DocumentProperty_Subject,                                 // Field - Document Property - Subject
        TUFE_DOCX_PresToken_Field_DocumentProperty_Template,                                // Field - Document Property - Template
        TUFE_DOCX_PresToken_Field_DocumentProperty_Title,                                   // Field - Document Property - Title
        TUFE_DOCX_PresToken_Field_DocumentAutomation_DocVariable,                           // Field - Document Automation - DocVariable
        TUFE_DOCX_PresToken_Field_DocumentAutomation_If,                                    // Field - Document Automation - If
        TUFE_DOCX_PresToken_Field_Numbering_AutoNumLgl,                                     // Field - Numbering - AutoNumLgl
        TUFE_DOCX_PresToken_Field_Numbering_AutoNumOut,                                     // Field - Numbering - AutoNumOut
        TUFE_DOCX_PresToken_Field_Numbering_RevNumFlags,                                    // Field - Numbering - RevNum Flags
        TUFE_DOCX_PresToken_Field_LinksAndReferences_IncludePicture,                        // Field - Links and References - Include Picture
        
        // [18]  Review
        TUFE_DOCX_PresToken_Review_Table_CellMerge,                                         // Review - Table - Cell Merge
        TUFE_DOCX_PresToken_Review_InsertedNumberingProperties,                             // Review - Inserted Numbering Properties
        TUFE_DOCX_PresToken_Review_Math_InsertedMathControlCharacter,                       // Review - Math - Inserted Math Control Character
        TUFE_DOCX_PresToken_Review_Math_DeletedMathControlCharacter,                        // Review - Math - Deleted Math Control Character
        TUFE_DOCX_PresToken_Review_RevisionInformationForSectionProperties,                 // Review - Revision Information for Section Properties
        TUFE_DOCX_PresToken_Review_RevisionInformationForTableGridColumnDefinitions,        // Review - Revision Information for Table Grid Column Definitions
        TUFE_DOCX_PresToken_Review_RevisionInformationForTableProperties,                   // Review - Revision Information for Table Properties
        TUFE_DOCX_PresToken_Review_RevisionInformationForTableLevelPropertyExceptions,      // Review - Revision Information for Table-Level Property Exceptions
        TUFE_DOCX_PresToken_Review_RevisionInformationForTableCellProperties,               // Review - Revision Information for Table Cell Properties
        TUFE_DOCX_PresToken_Review_RevisionInformationForTableRowProperties,                // Review - Revision Information for Table Row Properties
        
        // [19]  Document Protection
        TUFE_DOCX_PresToken_DocumentProtection_Formatting,                                  // Document Protection - Formatting
        TUFE_DOCX_PresToken_DocumentProtection_Enforcement,                                 // Document Protection - Enforcement
        TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowNoEditing,                         // Document Protection - Edit - Allow No Editing
        TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingOfComments,                 // Document Protection - Edit - Allow Editing of Comments
        TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingWithRevisionTracking,       // Document Protection - Edit - Allow Editing With Revision Tracking
        TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingOfFormFields,               // Document Protection - Edit - Allow Editing of Form Fields
        TUFE_DOCX_PresToken_DocumentProtection_ContentStatus_Final,                         // Document Protection - Content Status - Final
        TUFE_DOCX_PresToken_DocumentProtection_MarkAsFinal_True,                            // Document Protection - Mark As Final - True
        
        // [21]  Mail-Merge
        TUFE_DOCX_PresToken_MailMerge_MailMerge,                                            // Mail Merge - Mail Merge

        // [22]  Equestion & Symbol
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_LimitLocationSides,                      // Equation and Symbol - Math - Limit Location Sides
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_LogicalAnd,                        // Equation and Symbol - Math - N-Ary Logical And
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_LogicalOr,                         // Equation and Symbol - Math - N-Ary Logical Or
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_Intersection,                      // Equation and Symbol - Math - N-Ary Intersection
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_Union,                             // Equation and Symbol - Math - N-Ary Union
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_BorderBox,                               // Equation and Symbol - Math - Border-Box
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalDot,                   // Equation and Symbol - Math - Accent - Diacritical Dot
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DoubleDot,                        // Equation and Symbol - Math - Accent - Double Dot
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftArrow,             // Equation and Symbol - Math - Accent - Diacritical Left Arrow
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftRightArrow,        // Equation and Symbol - Math - Accent - Diacritical Left Right Arrow
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftVector,            // Equation and Symbol - Math - Accent - Diacritical Left Vector
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalRightVector,           // Equation and Symbol - Math - Accent - Diacritical Right Vector
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_Overline,                         // Equation and Symbol - Math - Accent - Overline
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_CombiningDoubleOverline,          // Equation and Symbol - Math - Accent - Combining Double Overline
        TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_UnsupportedAccent,                // Equation and Symbol - Math - Accent - Unsupported Accent

        // [23]  Style
        TUFE_DOCX_PresToken_Style_Table_TableRowProperties_TableCellSpacing,                // Style - Table - Table Row Properties - Table Cell Spacing
        TUFE_DOCX_PresToken_Style_Table_TableProperties_TableCellSpacing,                   // Style - Table - Table Properties - Table Cell Spacing
        TUFE_DOCX_PresToken_Style_Table_TableProperties_Alignment,                          // Style - Table - Table Properties - Alignment

        // [25]  Grouped Shapes
        TUFE_DOCX_PresToken_GroupedShape_GroupedShape_DrawingML,                            // Grouped Shape - Grouped Shape [DrawingML]
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Table,                              // Grouped Shape - Content in Shape - Table
//      TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Chart,                              // Grouped Shape - Content in Shape - Chart
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Drawing,                            // Grouped Shape - Content in Shape - Drawing
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_SdtContentControl,                  // Grouped Shape - Content in Shape - SDT Content Control
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_Insert,                // Grouped Shape - Content in Shape - Track Changes - Insert
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_Delete,                // Grouped Shape - Content in Shape - Track Changes - Delete
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_InsertParagraphMarker, // Grouped Shape - Content in Shape - Track Changes - Insert Paragraph Marker
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_DeleteParagraphMarker, // Grouped Shape - Content in Shape - Track Changes - Delete Paragraph Marker
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_ChangedParaProperties, // Grouped Shape - Content in Shape - Track Changes - Changed Para Properties
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_ChangedRunProperties,  // Grouped Shape - Content in Shape - Track Changes - Changed Run Properties
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_InlineEmbeddedObject,               // Grouped Shape - Content in Shape - Inline Embedded Object
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_ComplexFieldCharacter,              // Grouped Shape - Content in Shape - Complex Field Character
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_FieldCode,                          // Grouped Shape - Content in Shape - Field Code
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_SimpleField,                        // Grouped Shape - Content in Shape - Simple Field
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_MathTextInstance,                   // Grouped Shape - Content in Shape - Math Text Instance
        TUFE_DOCX_PresToken_GroupedShape_ContentInShape_MathParagraph,                      // Grouped Shape - Content in Shape - Math Paragraph
        TUFE_DOCX_PresToken_GroupedShape_GraphicFrameInGroupShape,                          // Grouped Shape - Graphic Frame in Group Shape

        // [29]  MISC
        TUFE_DOCX_PresToken_Misc_Macro,                                                     // Macro

        // Last
        TUFE_DOCX_PresToken_LAST                                                            // Semantic (prevent ',')
)


MLOEnum(TUFE_DOCX_PresNonToken_Enums,

        // [04]  Table
        TUFE_DOCX_PresNonToken_Table_VisuallyRightToLeft_InfoOnly,                          // Table - Visually Right-to-Left

        // [13]  Object
        TUFE_DOCX_PresNonToken_Object_OleObject,                                            // Object - OLE Object

        // [14]  Forms
        TUFE_DOCX_PresNonToken_Forms_ActiveX,                                               // Forms - ActiveX

        // [18]  Review
        TUFE_DOCX_PresNonToken_Review_InkAnnotations,                                       // Review - Ink Annotations
        TUFE_DOCX_PresNonToken_Review_MoveSource_ParagraphOrRunContent,                     // Review - Move Source [Paragraph / Run Content]
        TUFE_DOCX_PresNonToken_Review_MoveDestination_ParagraphOrRunContent,                // Review - Move Destination [Paragraph / Run Content]
        TUFE_DOCX_PresNonToken_Review_DeletedContentControl_Start,                          // Review - Content Control - Deleted Content Control [Start]
        TUFE_DOCX_PresNonToken_Review_DeletedContentControl_End,                            // Review - Content Control - Deleted Content Control [End]
        TUFE_DOCX_PresNonToken_Review_InsertedContentControl_Start,                         // Review - Content Control - Inserted Content Control [Start]
        TUFE_DOCX_PresNonToken_Review_InsertedContentControl_End,                           // Review - Content Control - Inserted Content Control [End]
        TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveSource_Start,                      // Review - Custom XML Markup - Move Source [Start]
        TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveSource_End,                        // Review - Custom XML Markup - Move Source [End]
        TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveDestination_Start,                 // Review - Custom XML Markup - Move Destination [Start]
        TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveDestination_End,                   // Review - Custom XML Markup - Move Destination [End]
        TUFE_DOCX_PresNonToken_Review_MoveSourceLocationContainer_Start,                    // Review - Move Source Location Container [Start]
        TUFE_DOCX_PresNonToken_Review_MoveSourceLocationContainer_End,                      // Review - Move Source Location Container [End]
        TUFE_DOCX_PresNonToken_Review_MoveDestinationLocationContainer_Start,               // Review - Move Destination Location Container [Start]
        TUFE_DOCX_PresNonToken_Review_MoveDestinationLocationContainer_End,                 // Review - Move Destination Location Container [End]
        TUFE_DOCX_PresNonToken_Review_DeletedFieldCode,                                     // Review - Deleted Field Code
        TUFE_DOCX_PresNonToken_Review_NumberingPropertiesChanges,                           // Review - Numbering Properties Changed
        
        // [19]  Document Protection
        TUFE_DOCX_PresNonToken_DocumentProtection_Edit_RangePermissionStart,                // Document Protection - Edit - Range Permission Start
        TUFE_DOCX_PresNonToken_DocumentProtection_Edit_RangePermissionEnd,                  // Document Protection - Edit - Range Permission End
        TUFE_DOCX_PresNonToken_DocumentProtection_Misc_DigitalSignature,                    // Document Protection - Misc - Digital Signature

        // [20]  Content-Control
        TUFE_DOCX_PresNonToken_ContentControl_General,                                      // Content Control - General

        // [22]  Equestion & Symbol
        TUFE_DOCX_PresNonToken_WordML_Symbol,                                               // WordML - Symbol

        // [26]  Canvas
        TUFE_DOCX_PresNonToken_Canvas_Canvas,                                               // Canvas - Canvas

        // [29]  MISC
        TUFE_DOCX_PresNonToken_Misc_SignatureLine,                                          // MISC - Signature Line

        // Last
        TUFE_DOCX_PresNonToken_LAST                                                         // Semantic (prevent ',')
)

MLOEnum(TUFE_DOCX_RndrToken_Enums,

        // [04]  Table
        TUFE_DOCX_RndrToken_Table_DeletedCell,                                              // Table - Deleted Cell
        TUFE_DOCX_RndrToken_Table_TableRowProperties_TableCellSpacing,                      // Table - Table Row Properties - Table Cell Spacing
        TUFE_DOCX_RndrToken_Table_TableProperties_TableCellSpacing,                         // Table - Table Properties - Table Cell Spacing
        TUFE_DOCX_RndrToken_Table_TableProperties_Alignment,                                // Table - Table Properties - Alignment

        // [05]  Chart
        TUFE_DOCX_RndrToken_Chart_TypePieOfPie,                                             // Chart - Pie of Pie
        TUFE_DOCX_RndrToken_Chart_TypeBarOfPie,                                             // Chart - Bar of Pie
        TUFE_DOCX_RndrToken_Chart_TypeSurface2D,                                            // Chart - Surface 2D
        TUFE_DOCX_RndrToken_Chart_TypeSurface3D,                                            // Chart - Surface 3D
        TUFE_DOCX_RndrToken_Chart_DataTable,                                                // Chart - Data Table
        TUFE_DOCX_RndrToken_Chart_DateAxis,                                                 // Chart - Date Axis

        // [06]  Text-Box
        TUFE_DOCX_RndrToken_TextBox_TextDirection_Vert,                                     // Text-Box - Vertical

        // [07]  Picture
        TUFE_DOCX_RndrToken_Picture_FlipHorizontal,                                         // Picture - Flip Horizontal
        TUFE_DOCX_RndrToken_Picture_FlipVertical,                                           // Picture - Flip Vertical

        // Generic Shape (Text-Box / Shape)
        TUFE_DOCX_RndrToken_GenericShape_FlipVertical,                                      // Generic Shape - Flip Vertical
        TUFE_DOCX_RndrToken_GenericShape_FontRefSchemeColor,                                // Generic Shape - Font Ref Scheme Color

        // Generic Drawing (Text-Box / Shape / Diagram)
        TUFE_DOCX_RndrToken_GenericDrawing_WrapType_Tight,                                  // Generic Drawing - Wrap Type - Tight
        TUFE_DOCX_RndrToken_GenericDrawing_WrapType_Through,                                // Generic Drawing - Wrap Type - Through
        TUFE_DOCX_RndrToken_GenericDrawing_WrapPolygon_Tight,                               // Generic Drawing - Wrap Polygon - Tight
        TUFE_DOCX_RndrToken_GenericDrawing_WrapPolygon_Through,                             // Generic Drawing - Wrap Polygon - Through

        // [09]  Diagram
        TUFE_DOCX_RndrToken_Diagram_SmartArt,                                               // Diagram - Smart-Art

        // [10]  Page-Setup
        TUFE_DOCX_RndrToken_PageSetup_Columns_LineBetween,                                  // Page-Setup - Columns - Line Between

        // [12]  Header-Footer
//      TUFE_DOCX_RndrToken_HeaderFooter_ShapeInHeaderFooter,                               // Header/Footer - Shape In Header/Footer

        // [15]  References
        TUFE_DOCX_RndrToken_References_SectionWideFootnoteProps_BeneathText,                // References - Section-Wide Footnote Properties - Beneath Text
        TUFE_DOCX_RndrToken_References_SectionWideEndnoteProps_EndOfSection,                // References - Section-Wide Endnote Properties - End of Section
        TUFE_DOCX_RndrToken_References_DocumentWideEndnoteProps_EndOfSection,               // References - Document-Wide Endnote Properties - End of Section
        TUFE_DOCX_RndrToken_References_SectionWideFootnoteEndnoteProps_RestartEachSection,  // References - Section-Wide Footnote / Endnote Properties - Restart Each Section
        TUFE_DOCX_RndrToken_References_DocumentWideFootnoteEndnoteProps_RestartEachSection, // References - Document-Wide Footnote / Endnote Properties - Restart Each Section

        // [20]  Content-Control
        TUFE_DOCX_RndrToken_ContentControl_ComboBox,                                        // Content Control - Combo-Box

        // [23]  Style
        TUFE_DOCX_RndrToken_Style_Table_TableRowProperties_TableCellSpacing,                // Style - Table - Table Row Properties - Table Cell Spacing
        TUFE_DOCX_RndrToken_Style_Table_TableProperties_TableCellSpacing,                   // Style - Table - Table Properties - Table Cell Spacing
        TUFE_DOCX_RndrToken_Style_Table_TableProperties_Alignment,                          // Style - Table - Table Properties - Alignment

        // [25]  Grouped Shapes
        TUFE_DOCX_RndrToken_GroupedShape_GroupedShape_DrawingML,                            // Grouped Shape - Grouped Shape [DrawingML]
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Table,                              // Grouped Shape - Content in Shape - Table
//      TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Chart,                              // Grouped Shape - Content in Shape - Chart
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Drawing,                            // Grouped Shape - Content in Shape - Drawing
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_SdtContentControl,                  // Grouped Shape - Content in Shape - SDT Content Control
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_Insert,                // Grouped Shape - Content in Shape - Track Changes - Insert
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_Delete,                // Grouped Shape - Content in Shape - Track Changes - Delete
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_InsertParagraphMarker, // Grouped Shape - Content in Shape - Track Changes - Insert Paragraph Marker
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_DeleteParagraphMarker, // Grouped Shape - Content in Shape - Track Changes - Delete Paragraph Marker
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_ChangedParaProperties, // Grouped Shape - Content in Shape - Track Changes - Changed Para Properties
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_ChangedRunProperties,  // Grouped Shape - Content in Shape - Track Changes - Changed Run Properties
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_InlineEmbeddedObject,               // Grouped Shape - Content in Shape - Inline Embedded Object
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_ComplexFieldCharacter,              // Grouped Shape - Content in Shape - Complex Field Character
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_FieldCode,                          // Grouped Shape - Content in Shape - Field Code
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_SimpleField,                        // Grouped Shape - Content in Shape - Simple Field
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_MathTextInstance,                   // Grouped Shape - Content in Shape - Math Text Instance
        TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_MathParagraph,                      // Grouped Shape - Content in Shape - Math Paragraph
        TUFE_DOCX_RndrToken_GroupedShape_GraphicFrameInGroupShape,                          // Grouped Shape - Graphic Frame in Group Shape

        // Last
        TUFE_DOCX_RndrToken_LAST                                                            // Semantic (prevent ',')
)

MLOEnum(TUFE_DOCX_RndrNonToken_Enums,

        // [26]  Canvas
        TUFE_DOCX_RndrNonToken_Canvas_Canvas,                                               // Canvas - Canvas

        // Last
        TUFE_DOCX_RndrNonToken_LAST                                                         // Semantic (prevent ',')
)

#ifdef IOS
void touch_ui_did_encounter_feature(TouchUiFeatureType featureType, int enumValue);
#else
static inline void touch_ui_did_encounter_feature(TouchUiFeatureType featureType, int enumValue)
{
    OUString featureTypeStr = "";
    switch (featureType)
    {
        case TUFE_DOCX_PresToken:                      featureTypeStr = "PRESERVATION     TOKENIZED";     break;
        case TUFE_DOCX_PresNonToken:                   featureTypeStr = "PRESERVATION NON-TOKENIZED";     break;
        case TUFE_DOCX_RndrToken:                      featureTypeStr = "RENDER           TOKENIZED";     break;
        case TUFE_DOCX_RndrNonToken:                   featureTypeStr = "RENDER       NON-TOKENIZED";     break;
        default:                                       featureTypeStr = "UKNOWN";                         break;
    };

    OUString enumValueStr = "";
    if ( featureType == TUFE_DOCX_PresToken )
    {
        switch (enumValue)
        {
            // [01]  Text
            case TUFE_DOCX_PresToken_Text_Kerning:                                                   enumValueStr = "Kerning";                                                                     break;
            
            // [02]  Paragraph
            case TUFE_DOCX_PresToken_Paragraph_Border_Between:                                       enumValueStr = "Paragraph - Border - Between";                                                break;
            case TUFE_DOCX_PresToken_Paragraph_Border_Bar:                                           enumValueStr = "Paragraph - Border - Bar";                                                    break;
//          case TUFE_DOCX_PresToken_Paragraph_Shading:                                              enumValueStr = "Paragraph - Shading";                                                         break;
            case TUFE_DOCX_PresToken_Paragraph_DropCap:                                              enumValueStr = "Paragraph - DropCap";                                                         break;
            case TUFE_DOCX_PresToken_Paragraph_PageBreakBefore:                                      enumValueStr = "Paragraph - Page-Break Before";                                               break;
            case TUFE_DOCX_PresToken_Paragraph_BarTab:                                               enumValueStr = "Paragraph - Bar Tab";                                                         break;
            case TUFE_DOCX_PresToken_Paragraph_DontHyphenate:                                        enumValueStr = "Paragraph - Don't Hyphenate";                                                 break;
            case TUFE_DOCX_PresToken_Paragraph_ContextualSpacingTurnedOff:                           enumValueStr = "Paragraph - Contextual Spacing Turned Off";                                   break;
            
            // [03]  List
            case TUFE_DOCX_PresToken_List_BulletColor:                                               enumValueStr = "List - Bullet Color";                                                         break;
            case TUFE_DOCX_PresToken_List_TextEffect_Outline:                                        enumValueStr = "List - Text Effect [Outline]";                                                break;
            case TUFE_DOCX_PresToken_List_TextEffect_Fill:                                           enumValueStr = "List - Text Effect [Fill]";                                                   break;
            case TUFE_DOCX_PresToken_List_TextEffect_Shadow:                                         enumValueStr = "List - Text Effect [Shadow]";                                                 break;
            case TUFE_DOCX_PresToken_List_TextEffect_Glow:                                           enumValueStr = "List - Text Effect [Glow]";                                                   break;
            case TUFE_DOCX_PresToken_List_TextEffect_Reflection:                                     enumValueStr = "List - Text Effect [Reflection]";                                             break;
            case TUFE_DOCX_PresToken_List_TextEffect_3DFormat:                                       enumValueStr = "List - Text Effect [3D-Format]";                                              break;
            case TUFE_DOCX_PresToken_List_OpenType_Ligatures:                                        enumValueStr = "List - OpenType Feature [Ligatures]";                                         break;
            case TUFE_DOCX_PresToken_List_OpenType_NumberSpacing:                                    enumValueStr = "List - OpenType Feature [Number Spacing]";                                    break;
            case TUFE_DOCX_PresToken_List_OpenType_NumberForms:                                      enumValueStr = "List - OpenType Feature [Number Forms]";                                      break;
            case TUFE_DOCX_PresToken_List_OpenType_StylisticSets:                                    enumValueStr = "List - OpenType Feature [Stylistic Sets]";                                    break;
            case TUFE_DOCX_PresToken_List_OpenType_ContexualAlternates:                              enumValueStr = "List - OpenType Feature [Contexual Alternates]";                              break;
            case TUFE_DOCX_PresToken_List_NumberedStyles_CardinalText:                               enumValueStr = "List - Numbered Styles - Cardinal Text";                                      break;
            case TUFE_DOCX_PresToken_List_NumberedStyles_Ordinal:                                    enumValueStr = "List - Numbered Styles - Ordinal";                                            break;
            case TUFE_DOCX_PresToken_List_NumberedStyles_OrdinalText:                                enumValueStr = "List - Numbered Styles - Ordinal Text";                                       break;
            case TUFE_DOCX_PresToken_List_NumberedStyles_ChicagoManualOfStyle:                       enumValueStr = "List - Numbered Styles - Chicago Manual of Style";                            break;
            case TUFE_DOCX_PresToken_List_LegalStyleNumbering:                                       enumValueStr = "List - Legal Style Numbering";                                                break;
            case TUFE_DOCX_PresToken_List_LevelPictureBulletId:                                      enumValueStr = "List - Level Picture Bullet Id";                                              break;

            // [04]  Table
            case TUFE_DOCX_PresToken_Table_DeletedCell:                                              enumValueStr = "Table - Deleted Cell";                                                        break;
//          case TUFE_DOCX_PresToken_Table_CellShadingValue:                                         enumValueStr = "Table - Cell Shading Value";                                                  break;
//          case TUFE_DOCX_PresToken_Table_ConditionalFormatting_TblLook:                            enumValueStr = "Table - Conditional Formatting";                                              break;
//          case TUFE_DOCX_PresToken_Table_ConditionalFormatting_CnfStyle:                           enumValueStr = "Table - CNF Style";                                                           break;
            case TUFE_DOCX_PresToken_Table_AlternativeText_Title:                                    enumValueStr = "Table - Alternative Text - Title";                                            break;
            case TUFE_DOCX_PresToken_Table_AlternativeText_Description:                              enumValueStr = "Table - Alternative Text - Description";                                      break;
            case TUFE_DOCX_PresToken_Table_TableRowProperties_TableCellSpacing:                      enumValueStr = "Table - Table Row Properties - Table Cell Spacing";                           break;
            case TUFE_DOCX_PresToken_Table_TableProperties_TableCellSpacing:                         enumValueStr = "Table - Table Properties - Table Cell Spacing";                               break;
            case TUFE_DOCX_PresToken_Table_TableProperties_Alignment:                                enumValueStr = "Table - Table Properties - Justification";                                    break;

            // [05]  Chart
            case TUFE_DOCX_PresToken_Chart_TypePieOfPie:                                             enumValueStr = "Chart - Pie of Pie";                                                          break;
            case TUFE_DOCX_PresToken_Chart_TypeBarOfPie:                                             enumValueStr = "Chart - Bar of Pie";                                                          break;
            case TUFE_DOCX_PresToken_Chart_TypeRadar:                                                enumValueStr = "Chart - Radar";                                                               break;
            case TUFE_DOCX_PresToken_Chart_TypeSurface2D:                                            enumValueStr = "Chart - Surface 2D";                                                          break;
            case TUFE_DOCX_PresToken_Chart_TypeSurface3D:                                            enumValueStr = "Chart - Surface 3D";                                                          break;
            case TUFE_DOCX_PresToken_Chart_TypeArea2D:                                               enumValueStr = "Chart - Area 2D";                                                             break;
            case TUFE_DOCX_PresToken_Chart_TypeScatter:                                              enumValueStr = "Chart - Scatter";                                                             break;
            case TUFE_DOCX_PresToken_Chart_TypeScatter_LineMarker:                                   enumValueStr = "Chart - Scatter - Line Marker";                                               break;
            case TUFE_DOCX_PresToken_Chart_TypeScatter_SmoothMarker:                                 enumValueStr = "Chart - Scatter - Smooth Marker";                                             break;
            case TUFE_DOCX_PresToken_Chart_TypeStock:                                                enumValueStr = "Chart - Stock";                                                               break;
            case TUFE_DOCX_PresToken_Chart_TypeLine:                                                 enumValueStr = "Chart - Line";                                                                break;
            case TUFE_DOCX_PresToken_Chart_DropLines:                                                enumValueStr = "Chart - Drop-Lines";                                                          break;
            case TUFE_DOCX_PresToken_Chart_HiLowLines:                                               enumValueStr = "Chart - Hi-Low-Lines";                                                        break;
            case TUFE_DOCX_PresToken_Chart_LegendOverlayTrue:                                        enumValueStr = "Chart - Legend Overlay True";                                                 break;
            case TUFE_DOCX_PresToken_Chart_DateAxis:                                                 enumValueStr = "Chart - Date Axis";                                                           break;

            // [06]  Text-Box
            case TUFE_DOCX_PresToken_TextBox_TextDirection_EaVert:                                   enumValueStr = "Text-Box - Text Direction - East Asian Vertical";                             break;
            case TUFE_DOCX_PresToken_TextBox_TextDirection_Vert:                                     enumValueStr = "Text-Box - Text Direction - Vertical";                                        break;
            case TUFE_DOCX_PresToken_TextBox_TextDirection_HorzNormEA:                               enumValueStr = "Text-Box - Text Direction - Horizontal (Normal East Asian Flow)";             break;
            case TUFE_DOCX_PresToken_TextBox_TextDirection_WordArtVert:                              enumValueStr = "Text-Box - Text Direction - WordArt Vertical";                                break;
            case TUFE_DOCX_PresToken_TextBox_TextDirection_MongolianVert:                            enumValueStr = "Text-Box - Text Direction - Mongolian Vertical";                              break;
            case TUFE_DOCX_PresToken_TextBox_TextDirection_WordArtVertRtl:                           enumValueStr = "Text-Box - Text Direction - Vertical WordArt Right to Left";                  break;
            case TUFE_DOCX_PresToken_TextBox_LinkedToOtherTextBox:                                   enumValueStr = "Text-Box - Linked to Other Text-Box";                                         break;
            case TUFE_DOCX_PresToken_TextBox_WrapTextInShape_None:                                   enumValueStr = "Text-Box - Wrap Text In Shape - None";                                        break;
            
            // [07]  Picture
            case TUFE_DOCX_PresToken_Picture_FlipHorizontal:                                         enumValueStr = "Picture - Flip Horizontal";                                                   break;
            case TUFE_DOCX_PresToken_Picture_FlipVertical:                                           enumValueStr = "Picture - Flip Vertical";                                                     break;
            case TUFE_DOCX_PresToken_Picture_Fill_Solid:                                             enumValueStr = "Picture - Fill - Solid";                                                      break;
            case TUFE_DOCX_PresToken_Picture_Fill_Gradient:                                          enumValueStr = "Picture - Fill - Gradient";                                                   break;
            case TUFE_DOCX_PresToken_Picture_Fill_Texture:                                           enumValueStr = "Picture - Fill - Texture";                                                    break;
            case TUFE_DOCX_PresToken_Picture_Fill_Pattern:                                           enumValueStr = "Picture - Fill - Pattern";                                                    break;
            case TUFE_DOCX_PresToken_Picture_Outline_Solid:                                          enumValueStr = "Picture - Outline - Solid";                                                   break;
            case TUFE_DOCX_PresToken_Picture_Outline_Gradient:                                       enumValueStr = "Picture - Outline - Gradient";                                                break;
            
            // Generic Shape (Text-Box / Shape)
            case TUFE_DOCX_PresToken_GenericShape_FlipHorizontal_InfoOnly:                           enumValueStr = "Generic Shape - Flip Horizontal";                                             break;
            case TUFE_DOCX_PresToken_GenericShape_FlipVertical_InfoOnly:                             enumValueStr = "Generic Shape - Flip Vertical";                                               break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_NotRotateWithShape:                  enumValueStr = "Generic Shape - Fill - Gradient - Not Rotate With Shape";                     break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_MoreThan2Colors:                     enumValueStr = "Generic Shape - Fill - Gradient - More Than 2 Colors";                        break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_Position:                            enumValueStr = "Generic Shape - Fill - Gradient - Position";                                  break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_FillToRect:                          enumValueStr = "Generic Shape - Fill - Gradient - Fill To Rect";                              break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_TileRect:                            enumValueStr = "Generic Shape - Fill - Gradient - Tile Rect";                                 break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_TypePath:                            enumValueStr = "Generic Shape - Fill - Gradient - Type Path";                                 break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Gradient_ColorManipulation:                   enumValueStr = "Generic Shape - Fill - Gradient - Color Manipulation";                        break;
            case TUFE_DOCX_PresToken_GenericShape_Fill_Pattern:                                      enumValueStr = "Generic Shape - Fill - Pattern";                                              break;
            case TUFE_DOCX_PresToken_GenericShape_Outline_Gradient:                                  enumValueStr = "Generic Shape - Outline - Gradient";                                          break;
            case TUFE_DOCX_PresToken_GenericShape_TextEffect_Transformation:                         enumValueStr = "Generic Shape - Text Effects - Transform";                                    break;
            case TUFE_DOCX_PresToken_GenericShape_FontRefSchemeColor:                                enumValueStr = "Generic Shape - Font Ref Scheme Color";                                       break;
            
            // Generic Drawing (Text-Box / Shape / Diagram)
            case TUFE_DOCX_PresToken_GenericDrawing_WrapType_Tight_InfoOnly:                         enumValueStr = "Generic Drawing - Wrap Type - Tight";                                         break;
            case TUFE_DOCX_PresToken_GenericDrawing_WrapType_Through_InfoOnly:                       enumValueStr = "Generic Drawing - Wrap Type - Through";                                       break;
            case TUFE_DOCX_PresToken_GenericDrawing_WrapPolygon_Tight_InfoOnly:                      enumValueStr = "Generic Drawing - Wrap Polygon - Tight";                                      break;
            case TUFE_DOCX_PresToken_GenericDrawing_WrapPolygon_Through_InfoOnly:                    enumValueStr = "Generic Drawing - Wrap Polygon - Through";                                    break;
            case TUFE_DOCX_PresToken_GenericDrawing_Hyperlink:                                       enumValueStr = "Generic Drawing - Hyperlink";                                                 break;
            case TUFE_DOCX_PresToken_GenericDrawing_AnchorLocked:                                    enumValueStr = "Generic Drawing - Anchor Locked";                                             break;
            
            // [09]  Diagram
            case TUFE_DOCX_PresToken_Diagram_SmartArt:                                               enumValueStr = "Diagram - Smart-Art";                                                         break;

            // [10]  Page-Setup
            case TUFE_DOCX_PresToken_PageSetup_Gutter:                                               enumValueStr = "Page-Setup - Gutter";                                                         break;
            case TUFE_DOCX_PresToken_PageSetup_2PagesPerSheet:                                       enumValueStr = "Page-Setup - 2 Pages Per Sheet";                                              break;
            case TUFE_DOCX_PresToken_PageSetup_BookFold:                                             enumValueStr = "Page-Setup - Book Fold";                                                      break;
            case TUFE_DOCX_PresToken_PageSetup_ReverseBookFold:                                      enumValueStr = "Page-Setup - Reverse Book Fold";                                              break;
//          case TUFE_DOCX_PresToken_PageSetup_MirrorMargins:                                        enumValueStr = "Page-Setup - Mirror Margins";                                                 break;
            case TUFE_DOCX_PresToken_PageSetup_AutoHyphenation:                                      enumValueStr = "Page-Setup - Auto Hyphenation";                                               break;
            case TUFE_DOCX_PresToken_PageSetup_DoNotHyphenateCaps:                                   enumValueStr = "Page-Setup - Do Not Hyphenate Caps";                                          break;
            case TUFE_DOCX_PresToken_PageSetup_ConsecutiveHyphenLimit:                               enumValueStr = "Page-Setup - Consecutive Hyphen Limit";                                       break;
            case TUFE_DOCX_PresToken_PageSetup_HyphenationZone:                                      enumValueStr = "Page-Setup - Hyphenation Zone";                                               break;
            case TUFE_DOCX_PresToken_PageSetup_RestartLineNumbersEachSection:                        enumValueStr = "Page-Setup - Restart Line Numbers Each Section";                              break;
            case TUFE_DOCX_PresToken_PageSetup_OnlyAllowEditingOfFormFieldsInSection_Off:            enumValueStr = "Page-Setup - Only Allow Editing Of Form Fields In Section - Off";             break;
            case TUFE_DOCX_PresToken_PageSetup_OnlyAllowEditingOfFormFieldsInSection_On:             enumValueStr = "Page-Setup - Only Allow Editing Of Form Fields In Section - On";              break;
            case TUFE_DOCX_PresToken_PageSetup_SectionBreak_Continuous:                              enumValueStr = "Page-Setup - Section Break - Continuous";                                     break;
            case TUFE_DOCX_PresToken_PageSetup_Columns_EqualWidthNotSet_ForMoreThanOne:              enumValueStr = "Page-Setup - Columns - Equal Width Not Set [For More Than One Column]";       break;
            case TUFE_DOCX_PresToken_PageSetup_Columns_MoreThan2Columns:                             enumValueStr = "Page-Setup - Columns - More than 2 Columns";                                  break;

            // [11]  Page Background
            case TUFE_DOCX_PresToken_PageBackground_PageColor_Solid:                                 enumValueStr = "Page Background - Page Color - Solid";                                        break;
            case TUFE_DOCX_PresToken_PageBackground_PageColor_LinearGradient:                        enumValueStr = "Page Background - Page Color - Linear Gradient";                              break;
            case TUFE_DOCX_PresToken_PageBackground_PageColor_RadialGradient:                        enumValueStr = "Page Background - Page Color - Radial Gradient";                              break;
            case TUFE_DOCX_PresToken_PageBackground_PageColor_TiledImage:                            enumValueStr = "Page Background - Page Color - Tiled Image";                                  break;
            case TUFE_DOCX_PresToken_PageBackground_PageColor_ImagePattern:                          enumValueStr = "Page Background - Page Color - Image Pattern";                                break;
            case TUFE_DOCX_PresToken_PageBackground_PageColor_StretchImageToFit:                     enumValueStr = "Page Background - Page Color - Stretch Image To Fit";                         break;
            case TUFE_DOCX_PresToken_PageBackground_PictureWatermark:                                enumValueStr = "Page Background - Picture Watermark";                                         break;
            case TUFE_DOCX_PresToken_PageBackground_TextWatermark:                                   enumValueStr = "Page Background - Text Watermark";                                            break;
            
            // Border
            case TUFE_DOCX_PresToken_BorderStyles_DotDashed:                                         enumValueStr = "Border Styles - Dot Dashed";                                                  break;
            case TUFE_DOCX_PresToken_BorderStyles_DotDotDashed:                                      enumValueStr = "Border Styles - Dot Dot Dashed";                                              break;
            case TUFE_DOCX_PresToken_BorderStyles_Triple:                                            enumValueStr = "Border Styles - Triple";                                                      break;
            case TUFE_DOCX_PresToken_BorderStyles_ThinThickThinMediumGap:                            enumValueStr = "Border Styles - Thin Thick Thin Medium Gap";                                  break;
            case TUFE_DOCX_PresToken_BorderStyles_Wave:                                              enumValueStr = "Border Styles - Wave";                                                        break;
            case TUFE_DOCX_PresToken_BorderStyles_DoubleWave:                                        enumValueStr = "Border Styles - Double Wave";                                                 break;
            case TUFE_DOCX_PresToken_BorderStyles_DashDotStroked:                                    enumValueStr = "Border Styles - Dash Dot Stroked";                                            break;
            case TUFE_DOCX_PresToken_BorderStyles_Outset:                                            enumValueStr = "Border Styles - Outset";                                                      break;
            case TUFE_DOCX_PresToken_BorderStyles_Inset:                                             enumValueStr = "Border Styles - Inset";                                                       break;
            
            // [12]  Header-Footer
            case TUFE_DOCX_PresToken_HeaderFooter_PositionalTab:                                     enumValueStr = "Header/Footer - Positional Tab";                                              break;
//          case TUFE_DOCX_PresToken_HeaderFooter_ShapeInHeaderFooter:                               enumValueStr = "Header/Footer - Shape In Header/Footer";                                      break;
//          case TUFE_DOCX_PresToken_HeaderFooter_ChartInHeaderFooter:                               enumValueStr = "Header/Footer - Chart In Header/Footer";                                      break;
            case TUFE_DOCX_PresToken_HeaderFooter_SdtContentInHeaderFooter_InfoOnly:                 enumValueStr = "Header/Footer - SDT Content In Header/Footer";                                break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_LowerRoman:                             enumValueStr = "Header/Footer - Page Number - Lower Roman";                                   break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_UpperRoman:                             enumValueStr = "Header/Footer - Page Number - Upper Roman";                                   break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_LowerAlphabetical:                      enumValueStr = "Header/Footer - Page Number - Lower Alphabetical";                            break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_UpperAlphabetical:                      enumValueStr = "Header/Footer - Page Number - Upper Alphabetical";                            break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_HyphenedDecimal:                        enumValueStr = "Header/Footer - Page Number - Hyphened Decimal";                              break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_ChapterStyle:                           enumValueStr = "Header/Footer - Page Number - Chapter Style";                                 break;
            case TUFE_DOCX_PresToken_HeaderFooter_PageNumber_ChapterSeparator:                       enumValueStr = "Header/Footer - Page Number - Chapter Separator";                             break;
            
            // [13]  Object
            case TUFE_DOCX_PresToken_Object_OleObject:                                               enumValueStr = "Object - OLE Object";                                                         break;
            case TUFE_DOCX_PresToken_Object_Type_Link:                                               enumValueStr = "Object - Type Link";                                                         break;
            case TUFE_DOCX_PresToken_Object_Excel_97_2003_Worksheet:                                 enumValueStr = "Object - Excel 97-2003 Worksheet";                                            break;
            case TUFE_DOCX_PresToken_Object_Excel_2010_Worksheet:                                    enumValueStr = "Object - Excel 2010 Worksheet";                                               break;
            case TUFE_DOCX_PresToken_Object_Excel_97_2003_Chart:                                     enumValueStr = "Object - Excel 97-2003 Chart";                                                break;
            case TUFE_DOCX_PresToken_Object_PowerPoint_97_2003_Presentation:                         enumValueStr = "Object - PowerPoint 97-2003 Presentation";                                    break;
            case TUFE_DOCX_PresToken_Object_PowerPoint_2010_Presentation:                            enumValueStr = "Object - PowerPoint 2010 Presentation";                                       break;
            case TUFE_DOCX_PresToken_Object_PowerPoint_97_2003_Slide:                                enumValueStr = "Object - PowerPoint 97-2003 Slide";                                           break;
            case TUFE_DOCX_PresToken_Object_PowerPoint_2010_Slide:                                   enumValueStr = "Object - PowerPoint 2010 Slide";                                              break;
            case TUFE_DOCX_PresToken_Object_Word_97_2003_Document:                                   enumValueStr = "Object - Word 97-2003 Document";                                              break;
            case TUFE_DOCX_PresToken_Object_Word_2010_Document:                                      enumValueStr = "Object - Word 2010 Document";                                                 break;
            case TUFE_DOCX_PresToken_Object_Word_OpenDocumentText_12:                                enumValueStr = "Object - Word OpenDocumentText 12";                                           break;
            case TUFE_DOCX_PresToken_Object_SoundRec:                                                enumValueStr = "Object - Sound Rec";                                                          break;
            case TUFE_DOCX_PresToken_Object_PBrush:                                                  enumValueStr = "Object - PBrush";                                                             break;
            case TUFE_DOCX_PresToken_Object_LinkToPackage:                                           enumValueStr = "Object - Link to Package";                                                    break;
            case TUFE_DOCX_PresToken_Object_DrawAsIcon:                                              enumValueStr = "Object - Draw As Icon";                                                       break;

            // [14]  Forms
            case TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_Type:                                  enumValueStr = "Forms - Legacy Form - Text-Box - Type";                                       break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_DefaultText:                           enumValueStr = "Forms - Legacy Form - Text-Box - Default Text";                               break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_MaxLength:                             enumValueStr = "Forms - Legacy Form - Text-Box - Max Length";                                 break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_TextBox_TextFormat:                            enumValueStr = "Forms - Legacy Form - Text-Box - Text Format";                                break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_Checkbox_Size:                                 enumValueStr = "Forms - Legacy Form - Check-Box - Size";                                      break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_Checkbox_DefaultValue:                         enumValueStr = "Forms - Legacy Form - Check-Box - Default Value";                             break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_Frame_TextFrameProperties_InBody:              enumValueStr = "Forms - Legacy Form - Frame - Text Frame Properties [In Body]";               break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_Frame_TextFrameProperties_InHeaderFooter:      enumValueStr = "Forms - Legacy Form - Frame - Text Frame Properties [In Header/Footer]";      break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_FillInDisabled:                   enumValueStr = "Forms - Legacy Form - Field Setting - Fill-In Disabled";                      break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_CalcOnExit:                       enumValueStr = "Forms - Legacy Form - Field Setting - Calc on Exit";                          break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_HelpText:                         enumValueStr = "Forms - Legacy Form - Field Setting - Help Text";                             break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_FieldSetting_StatusText:                       enumValueStr = "Forms - Legacy Form - Field Setting - Status Text";                           break;
            case TUFE_DOCX_PresToken_Forms_LegacyForm_DoNotShadeFormData:                            enumValueStr = "Forms - Legacy Form - Do Not Shade Form Data";                                break;

            // [15]  References
            case TUFE_DOCX_PresToken_References_Citation_Flag_L:                                     enumValueStr = "References - Citation - Flag L";                                              break;
            case TUFE_DOCX_PresToken_References_Citation_Flag_V:                                     enumValueStr = "References - Citation - Flag V";                                              break;
            case TUFE_DOCX_PresToken_References_Citation_Flag_F:                                     enumValueStr = "References - Citation - Flag F";                                              break;
            case TUFE_DOCX_PresToken_References_Citation_Flag_S:                                     enumValueStr = "References - Citation - Flag S";                                              break;
            case TUFE_DOCX_PresToken_References_Citation_Flag_M:                                     enumValueStr = "References - Citation - Flag M";                                              break;
            case TUFE_DOCX_PresToken_References_SectionWideFootnoteProps_BeneathText:                enumValueStr = "References - Section-Wide Footnote Properties - Beneath Text";                break;
            case TUFE_DOCX_PresToken_References_SectionWideFootnoteProps_EndOfSection:               enumValueStr = "References - Section-Wide Footnote Properties - End of Section";              break;
            case TUFE_DOCX_PresToken_References_SectionWideEndnoteProps_EndOfSection:                enumValueStr = "References - Section-Wide Endnote Properties - End of Section";               break;
            case TUFE_DOCX_PresToken_References_DocumentWideFootnoteProps_BeneathText:               enumValueStr = "References - Document-Wide Footnote Properties - Beneath Text";               break;
            case TUFE_DOCX_PresToken_References_DocumentWideFootnoteProps_EndOfSection:              enumValueStr = "References - Document-Wide Footnote Properties - End of Section";             break;
            case TUFE_DOCX_PresToken_References_DocumentWideEndnoteProps_EndOfSection:               enumValueStr = "References - Document-Wide Endnote Properties - End of Section";              break;
            case TUFE_DOCX_PresToken_References_FootnoteProps_NumberedStyles_ChicagoManualOfStyle:   enumValueStr = "References - Footnote Properties - Numbered Styles - Chicago Manual of Style";break;
            case TUFE_DOCX_PresToken_References_EndnoteProps_NumberedStyles_ChicagoManualOfStyle:    enumValueStr = "References - Endnote Properties - Numbered Styles - Chicago Manual of Style"; break;
            case TUFE_DOCX_PresToken_References_SectionWideFootnoteEndnoteProps_RestartEachPage:     enumValueStr = "References - Section-Wide Footnote / Endnote Properties - Restart Each Page"; break;
            case TUFE_DOCX_PresToken_References_SectionWideFootnoteEndnoteProps_RestartEachSection:  enumValueStr = "References - Section-Wide Footnote / Endnote Properties - Restart Each Section";  break;
            case TUFE_DOCX_PresToken_References_DocumentWideFootnoteEndnoteProps_RestartEachPage:    enumValueStr = "References - Document-Wide Footnote / Endnote Properties - Restart Each Page";break;
            case TUFE_DOCX_PresToken_References_DocumentWideFootnoteEndnoteProps_RestartEachSection: enumValueStr = "References - Document-Wide Footnote / Endnote Properties - Restart Each Section"; break;

            // [16]  Link
            case TUFE_DOCX_PresToken_Link_Tooltip:                                                   enumValueStr = "Link - Link - Tooltip";                                                       break;

            // [17]  Field
            case TUFE_DOCX_PresToken_Field_DateAndTime_EditTime:                                     enumValueStr = "Field - Date and Time - Edit Time";                                           break;
            case TUFE_DOCX_PresToken_Field_DateAndTime_CreateDate:                                   enumValueStr = "Field - Date and Time - Create Date";                                         break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Author:                                  enumValueStr = "Field - Document Property - Author";                                          break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Comments:                                enumValueStr = "Field - Document Property - Comments";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Filename:                                enumValueStr = "Field - Document Property - Filename";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Keywords:                                enumValueStr = "Field - Document Property - Keywords";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_LastSavedBy:                             enumValueStr = "Field - Document Property - Last Saved By";                                   break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_NumChars:                                enumValueStr = "Field - Document Property - NumChars";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_NumWords:                                enumValueStr = "Field - Document Property - NumWords";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_NumPages:                                enumValueStr = "Field - Document Property - NumPages";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Subject:                                 enumValueStr = "Field - Document Property - Subject";                                         break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Template:                                enumValueStr = "Field - Document Property - Template";                                        break;
            case TUFE_DOCX_PresToken_Field_DocumentProperty_Title:                                   enumValueStr = "Field - Document Property - Title";                                           break;
            case TUFE_DOCX_PresToken_Field_DocumentAutomation_DocVariable:                           enumValueStr = "Field - Document Automation - DocVariable";                                   break;
            case TUFE_DOCX_PresToken_Field_DocumentAutomation_If:                                    enumValueStr = "Field - Document Automation - If";                                            break;
            case TUFE_DOCX_PresToken_Field_Numbering_AutoNumLgl:                                     enumValueStr = "Field - Numbering - AutoNumLgl";                                              break;
            case TUFE_DOCX_PresToken_Field_Numbering_AutoNumOut:                                     enumValueStr = "Field - Numbering - AutoNumOut";                                              break;
            case TUFE_DOCX_PresToken_Field_Numbering_RevNumFlags:                                    enumValueStr = "Field - Numbering - RevNum Flags";                                            break;
            case TUFE_DOCX_PresToken_Field_LinksAndReferences_IncludePicture:                        enumValueStr = "Field - Links and References - Include Picture";                              break;
            
            // [18]  Review
            case TUFE_DOCX_PresToken_Review_Table_CellMerge:                                         enumValueStr = "Review - Table - Cell Merge";                                                 break;
            case TUFE_DOCX_PresToken_Review_InsertedNumberingProperties:                             enumValueStr = "Review - Inserted Numbering Properties";                                      break;
            case TUFE_DOCX_PresToken_Review_Math_InsertedMathControlCharacter:                       enumValueStr = "Review - Math - Inserted Math Control Character";                             break;
            case TUFE_DOCX_PresToken_Review_Math_DeletedMathControlCharacter:                        enumValueStr = "Review - Math - Deleted Math Control Character";                              break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForSectionProperties:                 enumValueStr = "Review - Revision Information for Section Properties";                        break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForTableGridColumnDefinitions:        enumValueStr = "Review - Revision Information for Table Grid Column Definitions";             break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForTableProperties:                   enumValueStr = "Review - Revision Information for Table Properties";                          break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForTableLevelPropertyExceptions:      enumValueStr = "Review - Revision Information for Table-Level Property Exceptions";           break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForTableCellProperties:               enumValueStr = "Review - Revision Information for Table Cell Properties";                     break;
            case TUFE_DOCX_PresToken_Review_RevisionInformationForTableRowProperties:                enumValueStr = "Review - Revision Information for Table Row Properties";                      break;
            
            // [19]  Document Protection
            case TUFE_DOCX_PresToken_DocumentProtection_Formatting:                                  enumValueStr = "Document Protection - Formatting";                                            break;
            case TUFE_DOCX_PresToken_DocumentProtection_Enforcement:                                 enumValueStr = "Document Protection - Enforcement";                                           break;
            case TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowNoEditing:                         enumValueStr = "Document Protection - Edit - Allow No Editing";                               break;
            case TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingOfComments:                 enumValueStr = "Document Protection - Edit - Allow Editing of Comments";                      break;
            case TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingWithRevisionTracking:       enumValueStr = "Document Protection - Edit - Allow Editing With Revision Tracking";           break;
            case TUFE_DOCX_PresToken_DocumentProtection_Edit_AllowEditingOfFormFields:               enumValueStr = "Document Protection - Edit - Allow Editing of Form Fields";                   break;
            case TUFE_DOCX_PresToken_DocumentProtection_ContentStatus_Final:                         enumValueStr = "Document Protection - Content Status - Final";                                break;
            case TUFE_DOCX_PresToken_DocumentProtection_MarkAsFinal_True:                            enumValueStr = "Document Protection - Mark As Final - True";                                  break;

            // [21]  Mail-Merge
            case TUFE_DOCX_PresToken_MailMerge_MailMerge:                                            enumValueStr = "Mail Merge - Mail Merge";                                                     break;

            // [22]  Equestion & Symbol
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_LimitLocationSides:                      enumValueStr = "Equation and Symbol - Math - Limit Location Sides";                           break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_LogicalAnd:                        enumValueStr = "Equation and Symbol - Math - N-Ary Logical And";                              break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_LogicalOr:                         enumValueStr = "Equation and Symbol - Math - N-Ary Logical Or";                               break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_Intersection:                      enumValueStr = "Equation and Symbol - Math - N-Ary Intersection";                             break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_N_Ary_Union:                             enumValueStr = "Equation and Symbol - Math - N-Ary Union";                                    break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_BorderBox:                               enumValueStr = "Equation and Symbol - Math - Border-Box" ;                                    break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalDot:                   enumValueStr = "Equation and Symbol - Math - Accent - Diacritical Dot";                       break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DoubleDot:                        enumValueStr = "Equation and Symbol - Math - Accent - Double Dot";                            break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftArrow:             enumValueStr = "Equation and Symbol - Math - Accent - Diacritical Left Arrow";                break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftRightArrow:        enumValueStr = "Equation and Symbol - Math - Accent - Diacritical Left Right Arrow";          break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalLeftVector:            enumValueStr = "Equation and Symbol - Math - Accent - Diacritical Left Vector";               break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_DiacriticalRightVector:           enumValueStr = "Equation and Symbol - Math - Accent - Diacritical Right Vector";              break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_Overline:                         enumValueStr = "Equation and Symbol - Math - Accent - Overline";                              break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_CombiningDoubleOverline:          enumValueStr = "Equation and Symbol - Math - Accent - Combining Double Overline";             break;
            case TUFE_DOCX_PresToken_EquationAndSymbol_Math_Accent_UnsupportedAccent:                enumValueStr = "Equation and Symbol - Math - Accent - Unsupported Accent";                    break;

            // [23]  Style
            case TUFE_DOCX_PresToken_Style_Table_TableRowProperties_TableCellSpacing:                enumValueStr = "Style - Table - Table Row Properties - Table Cell Spacing";                   break;
            case TUFE_DOCX_PresToken_Style_Table_TableProperties_TableCellSpacing:                   enumValueStr = "Style - Table - Table Properties - Table Cell Spacing";                       break;
            case TUFE_DOCX_PresToken_Style_Table_TableProperties_Alignment:                          enumValueStr = "Style - Table - Table Properties - Alignment";                                break;

            // [25]  Grouped Shapes
            case TUFE_DOCX_PresToken_GroupedShape_GroupedShape_DrawingML:                            enumValueStr = "Grouped Shape - Grouped Shape [DrawingML]";                                   break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Table:                              enumValueStr = "Grouped Shape - Content in Shape - Table";                                    break;
//          case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Chart:                              enumValueStr = "Grouped Shape - Content in Shape - Chart";                                    break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_Drawing:                            enumValueStr = "Grouped Shape - Content in Shape - Drawing";                                  break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_SdtContentControl:                  enumValueStr = "Grouped Shape - Content in Shape - SDT Content Control";                      break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_Insert:                enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Insert";                   break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_Delete:                enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Delete";                   break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_InsertParagraphMarker: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Insert Paragraph Marker";  break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_DeleteParagraphMarker: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Delete Paragraph Marker";  break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_ChangedParaProperties: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Changed Para Properties";  break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_TrackChanges_ChangedRunProperties:  enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Changed Run Properties";   break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_InlineEmbeddedObject:               enumValueStr = "Grouped Shape - Content in Shape - Inline Embedded Object";                   break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_ComplexFieldCharacter:              enumValueStr = "Grouped Shape - Content in Shape - Complex Field Character";                  break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_FieldCode:                          enumValueStr = "Grouped Shape - Content in Shape - Field Code";                               break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_SimpleField:                        enumValueStr = "Grouped Shape - Content in Shape - Simple Field";                             break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_MathTextInstance:                   enumValueStr = "Grouped Shape - Content in Shape - Math Text Instance";                       break;
            case TUFE_DOCX_PresToken_GroupedShape_ContentInShape_MathParagraph:                      enumValueStr = "Grouped Shape - Content in Shape - Math Paragraph";                           break;
            case TUFE_DOCX_PresToken_GroupedShape_GraphicFrameInGroupShape:                          enumValueStr = "Grouped Shape - Graphic Frame in Group Shape";                                break;

            // [29]  MISC
            case TUFE_DOCX_PresToken_Misc_Macro:                                                     enumValueStr = "Macro";                                                                       break;

            // Uknown
            default:                                                                                 enumValueStr = "UKNOWN";                                                                      break;
        };
    }
    else if ( featureType == TUFE_DOCX_PresNonToken )
    {
        switch (enumValue)
        {
            // [04]  Table
            case TUFE_DOCX_PresNonToken_Table_VisuallyRightToLeft_InfoOnly:                          enumValueStr = "Table - Visually Right-to-Left";                                              break;

            // [13]  Object
            case TUFE_DOCX_PresNonToken_Object_OleObject:                                            enumValueStr = "Object - OLE Object";                                                         break;

            // [14]  Forms
            case TUFE_DOCX_PresNonToken_Forms_ActiveX:                                               enumValueStr = "Forms - ActiveX";                                                             break;

            // [18]  Review
            case TUFE_DOCX_PresNonToken_Review_InkAnnotations:                                       enumValueStr = "Review - Ink Annotations";                                                    break;
            case TUFE_DOCX_PresNonToken_Review_MoveSource_ParagraphOrRunContent:                     enumValueStr = "Review - Move Source [Paragraph / Run Content]";                              break;
            case TUFE_DOCX_PresNonToken_Review_MoveDestination_ParagraphOrRunContent:                enumValueStr = "Review - Move Destination [Paragraph / Run Content]";                         break;
            case TUFE_DOCX_PresNonToken_Review_DeletedContentControl_Start:                          enumValueStr = "Review - Content Control - Deleted Content Control [Start]";                  break;
            case TUFE_DOCX_PresNonToken_Review_DeletedContentControl_End:                            enumValueStr = "Review - Content Control - Deleted Content Control [End]";                    break;
            case TUFE_DOCX_PresNonToken_Review_InsertedContentControl_Start:                         enumValueStr = "Review - Content Control - Inserted Content Control [Start]";                 break;
            case TUFE_DOCX_PresNonToken_Review_InsertedContentControl_End:                           enumValueStr = "Review - Content Control - Inserted Content Control [End]";                   break;
            case TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveSource_Start:                      enumValueStr = "Review - Custom XML Markup - Move Source [Start]";                            break;
            case TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveSource_End:                        enumValueStr = "Review - Custom XML Markup - Move Source [End]";                              break;
            case TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveDestination_Start:                 enumValueStr = "Review - Custom XML Markup - Move Destination [Start]";                       break;
            case TUFE_DOCX_PresNonToken_Review_CustomXmlMarkupMoveDestination_End:                   enumValueStr = "Review - Custom XML Markup - Move Destination [End]";                         break;
            case TUFE_DOCX_PresNonToken_Review_MoveSourceLocationContainer_Start:                    enumValueStr = "Review - Move Source Location Container [Start]";                             break;
            case TUFE_DOCX_PresNonToken_Review_MoveSourceLocationContainer_End:                      enumValueStr = "Review - Move Source Location Container [End]";                               break;
            case TUFE_DOCX_PresNonToken_Review_MoveDestinationLocationContainer_Start:               enumValueStr = "Review - Move Destination Location Container [Start]";                        break;
            case TUFE_DOCX_PresNonToken_Review_MoveDestinationLocationContainer_End:                 enumValueStr = "Review - Move Destination Location Container [End]";                          break;
            case TUFE_DOCX_PresNonToken_Review_DeletedFieldCode:                                     enumValueStr = "Review - Deleted Field Code";                                                 break;
            case TUFE_DOCX_PresNonToken_Review_NumberingPropertiesChanges:                           enumValueStr = "Review - Numbering Properties Changed";                                       break;
            
            // [19]  Document Protection
            case TUFE_DOCX_PresNonToken_DocumentProtection_Edit_RangePermissionStart:                enumValueStr = "Document Protection - Edit - Range Permission Start";                         break;
            case TUFE_DOCX_PresNonToken_DocumentProtection_Edit_RangePermissionEnd:                  enumValueStr = "Document Protection - Edit - Range Permission End";                           break;
            case TUFE_DOCX_PresNonToken_DocumentProtection_Misc_DigitalSignature:                    enumValueStr = "Document Protection - Misc - Digital Signature";                              break;

            // [20]  Content-Control
            case TUFE_DOCX_PresNonToken_ContentControl_General:                                      enumValueStr = "Content Control - General";                                                   break;

            // [22]  Equestion & Symbol
            case TUFE_DOCX_PresNonToken_WordML_Symbol:                                               enumValueStr = "WordML - Symbol";                                                             break;

            // [26]  Canvas
            case TUFE_DOCX_PresNonToken_Canvas_Canvas:                                               enumValueStr = "Canvas - Canvas";                                                             break;

            // [29]  MISC
            case TUFE_DOCX_PresNonToken_Misc_SignatureLine:                                          enumValueStr = "MISC - Signature Line";                                                       break;

            // Unknown
            default:                                                                                 enumValueStr = "UKNOWN";                                                                      break;
        };
    }
    else if ( featureType == TUFE_DOCX_RndrToken )
    {
        switch (enumValue)
        {
            // [04]  Table
            case TUFE_DOCX_RndrToken_Table_DeletedCell:                                              enumValueStr = "Table - Deleted Cell";                                                        break;
            case TUFE_DOCX_RndrToken_Table_TableRowProperties_TableCellSpacing:                      enumValueStr = "Table - Table Row Properties - Table Cell Spacing";                           break;
            case TUFE_DOCX_RndrToken_Table_TableProperties_TableCellSpacing:                         enumValueStr = "Table - Table Properties - Table Cell Spacing";                               break;
            case TUFE_DOCX_RndrToken_Table_TableProperties_Alignment:                                enumValueStr = "Table - Table Properties - Alignment";                                        break;

            // [05]  Chart
            case TUFE_DOCX_RndrToken_Chart_TypePieOfPie:                                             enumValueStr = "Chart - Pie of Pie";                                                          break;
            case TUFE_DOCX_RndrToken_Chart_TypeBarOfPie:                                             enumValueStr = "Chart - Bar of Pie";                                                          break;
            case TUFE_DOCX_RndrToken_Chart_TypeSurface2D:                                            enumValueStr = "Chart - Surface 2D";                                                          break;
            case TUFE_DOCX_RndrToken_Chart_TypeSurface3D:                                            enumValueStr = "Chart - Surface 3D";                                                          break;
            case TUFE_DOCX_RndrToken_Chart_DataTable:                                                enumValueStr = "Chart - Data Table";                                                          break;
            case TUFE_DOCX_RndrToken_Chart_DateAxis:                                                 enumValueStr = "Chart - Date Axis";                                                           break;

            // [06]  Text-Box
            case TUFE_DOCX_RndrToken_TextBox_TextDirection_Vert:                                     enumValueStr = "Text-Box - Text Direction - Vertical";                                        break;

            // [07]  Picture
            case TUFE_DOCX_RndrToken_Picture_FlipHorizontal:                                         enumValueStr = "Picture - Flip Horizontal";                                                   break;
            case TUFE_DOCX_RndrToken_Picture_FlipVertical:                                           enumValueStr = "Picture - Flip Vertical";                                                     break;

            // Generic Shape (Text-Box / Shape)
            case TUFE_DOCX_RndrToken_GenericShape_FlipVertical:                                      enumValueStr = "Generic Shape - Flip Vertical";                                               break;
            case TUFE_DOCX_RndrToken_GenericShape_FontRefSchemeColor:                                enumValueStr = "Generic Shape - Font Ref Scheme Color";                                       break;

            // Generic Drawing (Text-Box / Shape / Diagram)
            case TUFE_DOCX_RndrToken_GenericDrawing_WrapType_Tight:                                  enumValueStr = "Generic Drawing - Wrap Type - Tight";                                         break;
            case TUFE_DOCX_RndrToken_GenericDrawing_WrapType_Through:                                enumValueStr = "Generic Drawing - Wrap Type - Through";                                       break;
            case TUFE_DOCX_RndrToken_GenericDrawing_WrapPolygon_Tight:                               enumValueStr = "Generic Drawing - Wrap Polygon - Tight";                                      break;
            case TUFE_DOCX_RndrToken_GenericDrawing_WrapPolygon_Through:                             enumValueStr = "Generic Drawing - Wrap Polygon - Through";                                    break;

            // [09]  Diagram
            case TUFE_DOCX_RndrToken_Diagram_SmartArt:                                               enumValueStr = "Diagram - Smart-Art";                                                         break;

            // [10]  Page-Setup
            case TUFE_DOCX_RndrToken_PageSetup_Columns_LineBetween:                                  enumValueStr = "Page-Setup - Columns - Line Between";                                         break;

            // [12]  Header-Footer
//          case TUFE_DOCX_RndrToken_HeaderFooter_ShapeInHeaderFooter:                               enumValueStr = "Header/Footer - Shape In Header/Footer";                                      break;

            // [15]  References
            case TUFE_DOCX_RndrToken_References_SectionWideFootnoteProps_BeneathText:                enumValueStr = "References - Section-Wide Footnote Properties - Beneath Text";                break;
            case TUFE_DOCX_RndrToken_References_SectionWideEndnoteProps_EndOfSection:                enumValueStr = "References - Document-Wide Endnote Properties - End of Section";              break;
            case TUFE_DOCX_RndrToken_References_DocumentWideEndnoteProps_EndOfSection:               enumValueStr = "References - Document-Wide Endnote Properties - End of Section";              break;
            case TUFE_DOCX_RndrToken_References_SectionWideFootnoteEndnoteProps_RestartEachSection:  enumValueStr = "References - Section-Wide Footnote / Endnote Properties - Restart Each Section";  break;
            case TUFE_DOCX_RndrToken_References_DocumentWideFootnoteEndnoteProps_RestartEachSection: enumValueStr = "References - Document-Wide Footnote / Endnote Properties - Restart Each Section"; break;

            // [20]  Content-Control
            case TUFE_DOCX_RndrToken_ContentControl_ComboBox:                                        enumValueStr = "Content Control - Combo-Box";                                                 break;

            // [23]  Style
            case TUFE_DOCX_RndrToken_Style_Table_TableRowProperties_TableCellSpacing:                enumValueStr = "Style - Table - Table Row Properties - Table Cell Spacing";                   break;
            case TUFE_DOCX_RndrToken_Style_Table_TableProperties_TableCellSpacing:                   enumValueStr = "Style - Table - Table Properties - Table Cell Spacing";                       break;
            case TUFE_DOCX_RndrToken_Style_Table_TableProperties_Alignment:                          enumValueStr = "Style - Table - Table Properties - Alignment";                                break;

            // [25]  Grouped Shapes
            case TUFE_DOCX_RndrToken_GroupedShape_GroupedShape_DrawingML:                            enumValueStr = "Grouped Shape - Grouped Shape [DrawingML]";                                   break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Table:                              enumValueStr = "Grouped Shape - Content in Shape - Table";                                    break;
//          case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Chart:                              enumValueStr = "Grouped Shape - Content in Shape - Chart";                                    break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_Drawing:                            enumValueStr = "Grouped Shape - Content in Shape - Drawing";                                  break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_SdtContentControl:                  enumValueStr = "Grouped Shape - Content in Shape - SDT Content Control";                      break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_Insert:                enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Insert";                   break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_Delete:                enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Delete";                   break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_InsertParagraphMarker: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Insert Paragraph Marker";  break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_DeleteParagraphMarker: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Delete Paragraph Marker";  break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_ChangedParaProperties: enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Changed Para Properties";  break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_TrackChanges_ChangedRunProperties:  enumValueStr = "Grouped Shape - Content in Shape - Track Changes - Changed Run Properties";   break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_InlineEmbeddedObject:               enumValueStr = "Grouped Shape - Content in Shape - Inline Embedded Object";                   break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_ComplexFieldCharacter:              enumValueStr = "Grouped Shape - Content in Shape - Complex Field Character";                  break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_FieldCode:                          enumValueStr = "Grouped Shape - Content in Shape - Field Code";                               break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_SimpleField:                        enumValueStr = "Grouped Shape - Content in Shape - Simple Field";                             break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_MathTextInstance:                   enumValueStr = "Grouped Shape - Content in Shape - Math Text Instance";                       break;
            case TUFE_DOCX_RndrToken_GroupedShape_ContentInShape_MathParagraph:                      enumValueStr = "Grouped Shape - Content in Shape - Math Paragraph";                           break;
            case TUFE_DOCX_RndrToken_GroupedShape_GraphicFrameInGroupShape:                          enumValueStr = "Grouped Shape - Graphic Frame in Group Shape";

            // Unknown
            default:                                                                                 enumValueStr = "UKNOWN";                                                                      break;
        };
    }
    else if ( featureType == TUFE_DOCX_RndrNonToken )
    {
        switch (enumValue)
        {
            // [26]  Canvas
            case TUFE_DOCX_RndrNonToken_Canvas_Canvas:                                               enumValueStr = "Canvas - Canvas";                                                             break;

            // Unknown
            default:                                                                                 enumValueStr = "UKNOWN";                                                                      break;
        };
    }
    
    SAL_DEBUG("     touch_ui_did_encounter_feature  -  " << featureTypeStr << "    <<    " << enumValueStr);
}
#endif

#ifdef __cplusplus
}
#endif

#endif // INCLUDED_TOUCH_TOUCH_CONTENT_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
