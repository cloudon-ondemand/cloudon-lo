//
//  touch-enums.h
//  ios_sharedlo
//
//  Created by ptyl on 4/11/14.
//  Copyright (c) 2014 LibreOffice.org. All rights reserved.
//
#ifndef __OBJC__
#ifdef __cplusplus
#define MLO_ENUM_SUPPORT_FOR_CPP
#define MLO_ENUM_SUPPORT_FOR_OU_STRING
#endif /* __cplusplus */
#endif /* __OBJC__ */


#undef MLOAllStringFromEnum
#undef MLOCppStringFromEnum
#undef MLOStringFromEnum
#undef MLOEnum
#undef MLONSEnum
#undef MLOStdStringFromEnum

#ifndef MLOAllEnumValuesString
#define MLOAllEnumValuesString(ENUM_TYPE,...)\
static const char * ALL_##ENUM_TYPE##_VALUES = #__VA_ARGS__;\
static const char * DEFAULT##ENUM_TYPE##_VALUE = #ENUM_TYPE "_UNKOWN_VALUE_";
#endif /* MLOEnumConstants*/

#ifdef MLO_ENUM_SUPPORT_FOR_CPP

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

#ifdef MLO_ENUM_SUPPORT_FOR_OU_STRING

#ifndef RTL_USING
#define RTL_USING
#endif /* RTL_USING */

#include "rtl/ustring.hxx"

#endif /* MLO_ENUM_SUPPORT_FOR_OU_STRING */


#ifndef __MLOEnumConverter__
#define __MLOEnumConverter__

class MLOEnumConverter {
    std::string * intToString;
    unsigned long enumKeysCount;
    std::string defaultValueString;
public:
    MLOEnumConverter(const char * allValues,
                     const char * defaultValue):
    intToString(nullptr),
    defaultValueString(defaultValue)
    {
        using namespace std;
        string allValuesString(allValues);
        allValuesString.erase(remove(allValuesString.begin(),
                                     allValuesString.end(),
                                     ' '),
                              allValuesString.end());
        istringstream stringStream1(allValuesString);
        string enumStringValue;
        int count = 0;
        while (getline(stringStream1,
                       enumStringValue,
                       ','))
        {
            count++;
        }
        enumKeysCount = count;
        intToString = new string[count];
        count = 0;
        istringstream stringStream2(allValuesString);
        while (getline(stringStream2,
                       enumStringValue,
                       ','))
        {
            intToString[count++]=enumStringValue;
        }
    };
    ~MLOEnumConverter(){
        if(intToString){
            delete [] intToString;
        }
    }
    int intFromStdString(std::string enumString,
                         int defaultValue) const
    {
        for(unsigned long i = 0; i < enumKeysCount; ++i){
            if(intToString[(int)i] == enumString){
                return (int) i;
            }
        }
        return defaultValue;
    }
    const std::string & stdStringFromInt(unsigned long key) const
    {
        if(key < enumKeysCount)
        {
            return intToString[key];
        }
        return defaultValueString;
    }
#ifdef MLO_ENUM_SUPPORT_FOR_OU_STRING
    int intFromOUString(const OUString & ouString,
                        int defaultValue) const
    {
        auto oString = OUStringToOString(ouString,RTL_TEXTENCODING_UTF8);
        std::string string(oString.getStr());
        return intFromStdString(string,
                                defaultValue);
    }
    OUString ouStringFromInt(unsigned long key) const
    {
        auto stdString = stdStringFromInt(key);
        return OStringToOUString(OString(stdString.c_str()),
                                 RTL_TEXTENCODING_UTF8);
    }
#endif /* MLO_ENUM_SUPPORT_FOR_OU_STRING */
};
#endif /* __MLOEnumConverter__ */

#define MLOStdStringFromEnum(ENUM_TYPE)\
static inline const MLOEnumConverter & get##ENUM_TYPE##Converter()\
{\
/**/static const MLOEnumConverter instance{ALL_##ENUM_TYPE##_VALUES,DEFAULT##ENUM_TYPE##_VALUE};\
/**/return instance;\
}\
static inline const std::string & ENUM_TYPE##StdString(ENUM_TYPE enumValue) { return get##ENUM_TYPE##Converter().stdStringFromInt((unsigned long)enumValue); }\
static inline ENUM_TYPE ENUM_TYPE##FromStdString(std::string str, ENUM_TYPE defaultValue) { return (ENUM_TYPE) get##ENUM_TYPE##Converter().intFromStdString(str,defaultValue); }

#ifdef MLO_ENUM_SUPPORT_FOR_OU_STRING

#define MLOCppStringFromEnum(ENUM_TYPE) MLOStdStringFromEnum(ENUM_TYPE)\
static inline OUString ENUM_TYPE##OUString(ENUM_TYPE enumValue) { return get##ENUM_TYPE##Converter().ouStringFromInt((unsigned long)enumValue); }\
static inline ENUM_TYPE ENUM_TYPE##FromOUString(OUString str, ENUM_TYPE defaultValue) { return (ENUM_TYPE) get##ENUM_TYPE##Converter().intFromOUString(str,defaultValue); }

#else /*MLO_ENUM_SUPPORT_FOR_OU_STRING*/

#define MLOCppStringFromEnum(ENUM_TYPE) MLOStdStringFromEnum(ENUM_TYPE)

#endif /* MLO_ENUM_SUPPORT_FOR_OU_STRING */

#else /* MLO_ENUM_SUPPORT_FOR_CPP */

#define MLOCppStringFromEnum(ENUM_TYPE,...)

#endif /* MLO_ENUM_SUPPORT_FOR_CPP */

#ifdef __OBJC__

#ifndef __mlo_enum_objective_c_helper_functions__
#define __mlo_enum_objective_c_helper_functions__

#include "touch-sal.h"

#endif /* __mlo_enum_objective_c_helper_functions__ */

#define MLOAllStringFromEnum(ENUM_TYPE,PREFIX,...) \
static int ENUM_TYPE##EnumId = -1;\
PREFIX NSString * ENUM_TYPE##String(ENUM_TYPE a##ENUM_TYPE) \
{\
/**/return (__bridge NSString *) mlo_string_from_enum_value((int) a##ENUM_TYPE,\
/*                                                        */&ENUM_TYPE##EnumId,\
/*                                                        */ALL_##ENUM_TYPE##_VALUES,\
/*                                                        */DEFAULT##ENUM_TYPE##_VALUE);\
}\
PREFIX ENUM_TYPE ENUM_TYPE##FromString(NSString * stringedEnum, ENUM_TYPE defaultValue)\
{\
/**/return (ENUM_TYPE) mlo_enum_from_enum_string_value((__bridge void *) stringedEnum,\
/*                                                   */(int)defaultValue,\
/*                                                   */&ENUM_TYPE##EnumId,\
/*                                                   */ALL_##ENUM_TYPE##_VALUES,\
/*                                                   */DEFAULT##ENUM_TYPE##_VALUE);\
}\
MLOCppStringFromEnum(ENUM_TYPE)

#define MLOStringFromEnum(ENUM_TYPE,...) MLOAllEnumValuesString(ENUM_TYPE,__VA_ARGS__) MLOAllStringFromEnum(ENUM_TYPE,static inline,__VA_ARGS__)
#define MLOStringFromEnumImpl(ENUM_TYPE,...) MLOAllEnumValuesString(ENUM_TYPE,__VA_ARGS__) MLOAllStringFromEnum(ENUM_TYPE,,__VA_ARGS__)
#else /*__OBJC__*/

#define MLOStringFromEnum(ENUM_TYPE,...) MLOAllEnumValuesString(ENUM_TYPE,__VA_ARGS__) MLOCppStringFromEnum(ENUM_TYPE)

#endif /*__OBJC__*/

#define MLOEnum(ENUM_TYPE,...) typedef enum { __VA_ARGS__ } ENUM_TYPE; MLOStringFromEnum(ENUM_TYPE,__VA_ARGS__)

#define MLONSEnum(NUMERIC_TYPE,ENUM_TYPE,...)  typedef NS_ENUM(NUMERIC_TYPE,ENUM_TYPE){ __VA_ARGS__ }; MLOStringFromEnum(ENUM_TYPE,__VA_ARGS__)
