/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * Copyright 2013 LibreOffice contributors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef INCLUDED_TOUCH_TOUCH_HOOKS_H
#define INCLUDED_TOUCH_TOUCH_HOOKS_H

#ifdef IOS

// #define TOUCH_UI_WRAP_MAIN_WITH_TRY_CATCH
// with this define commented, we can catch thrown objects from within
// libreoffice's main thread

#define TouchUiFailAssert(MESSAGE) touch_ui_assert_failed(#MESSAGE,__func__)

// ON_FAIL is first to allow easier replacing of assert statements
#define TouchUiAssert(ON_FAIL,PREDICATE)\
if(!(PREDICATE))\
{\
/**/TouchUiFailAssert(#PREDICATE);\
/**/ON_FAIL\
}


#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif /* 0 */
#endif /* __cplusplus */

void touch_ui_did_fail_to_load(void * pOUString);
void touch_ui_did_begin_to_load();
bool touch_ui_is_allow_rendering();
void touch_ui_log_debug(const char * message);
void touch_ui_log_info(const char * message);
void touch_ui_log_warning(const char * message);
void touch_ui_log_error(const char * message);
void touch_ui_assert_failed(const char * predicateAsString, const char * functionName);
bool touch_ui_is_closing();

// #define MLO_ENABLE_TOUCH_UI_CRASH_LOGGING
// with this disabled, all TouchUiCrash macros, and touch_ui_log_crash are inaccessible

#ifdef MLO_ENABLE_TOUCH_UI_CRASH_LOGGING

#define TouchUiCrashLog(CRASH_ID)\
touch_ui_log_crash(CRASH_ID,__FILE__,__func__,__LINE__,nullptr)

#define TouchUiCrashMessage(CRASH_ID,MESSAGE)\
touch_ui_log_crash(CRASH_ID,__FILE__,__func__,__LINE__,MESSAGE)

#define TouchUiCrashTest(CRASH_ID,TESTED_CONDITION)\
TouchUiCrashMessage(CRASH_ID,"will test " #TESTED_CONDITION) ?\
(TESTED_CONDITION) ? \
TouchUiCrashMessage(CRASH_ID,"condition is true") : \
TouchUiCrashMessage(CRASH_ID,"condition is false") : false

bool touch_ui_log_crash(int crashId, const char * fileName, const char * functionName, int lineNumber, const char * message);

#endif /* MLO_ENABLE_TOUCH_UI_CRASH_LOGGING */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#else /* not IOS */

#define TOUCH_UI_WRAP_MAIN_WITH_TRY_CATCH

#define touch_ui_did_fail_to_load(pOUString)
#define touch_ui_did_begin_to_load()
#define touch_ui_is_allow_rendering() true
#define touch_ui_log_debug(message)
#define touch_ui_log_info(message)
#define touch_ui_log_warning(message)
#define touch_ui_log_error(message)
#define touch_ui_assert_failed(predicateAsString, functionName)
#define TouchUiAssert(ON_FAIL,PREDICATE)
#define TouchUiFailAssert(MESSAGE)
#define touch_ui_is_closing() false

#endif /* IOS */

// MLO_ENABLE_TOUCH_UI_CRASH_LOGGING is not defined, if disabled, or if is not IOS
// in such a case, all macros and funcitons are tuned to nothing

#ifndef MLO_ENABLE_TOUCH_UI_CRASH_LOGGING

#ifndef MLO_DISABLE_TOUCH_UI_CRASH_LOGGING
#define MLO_DISABLE_TOUCH_UI_CRASH_LOGGING

#define touch_ui_log_crash(crashId,fileName,functionName,lineNumber,message)
#define TouchUiCrashLog(CRASH_ID)
#define TouchUiCrashMessage(CRASH_ID,MESSAGE)
#define TouchUiCrashTest(CRASH_ID,TESTED_CONDITION)

#endif /* MLO_DISABLE_TOUCH_UI_CRASH_LOGGING */

#endif /* MLO_ENABLE_TOUCH_UI_CRASH_LOGGING */

#endif // INCLUDED_TOUCH_TOUCH_HOOKS_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
