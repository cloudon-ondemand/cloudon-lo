/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * Copyright 2013 LibreOffice contributors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INCLUDED_TOUCH_TOUCH_SAL_H
#define INCLUDED_TOUCH_TOUCH_SAL_H

#include "touch/touch-common.h"

#ifdef IOS
#include <premac.h>
#include <CoreGraphics/CoreGraphics.h>
#include <postmac.h>
#else
#include <basegfx/range/b2ibox.hxx>
#include <basegfx/range/b1drange.hxx>
#include <basegfx/range/b2drange.hxx>
#include <basegfx/tuple/b2dtuple.hxx>
#endif

#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif
#endif

// =========================
// Operating System specific
// =========================

#ifdef IOS
typedef CGRect MLODpxRect;
// coordinate types

typedef CGFloat MLODpx;
typedef CGPoint MLODpxPoint;
typedef CGSize MLODpxSize;

// coordinate constants

#define MLODpxPointZero CGPointZero
#define MLODpxSizeZero CGSizeZero
#define MLODpxRectZero CGRectZero

CG_INLINE MLODpxPoint MLODpxPointByDpxes(MLODpx x, MLODpx y)
{
    return CGPointMake(x, y);
}

CG_INLINE MLODpxSize MLODpxSizeByDpxes(MLODpx width, MLODpx height)
{
    return CGSizeMake(width, height);
}

CG_INLINE MLODpxRect MLODpxRectByDpxes(MLODpx x, MLODpx y, MLODpx width, MLODpx height)
{
    return CGRectMake(x,
                      y,
                      width,
                      height);
}

static const MLORip LO_TWIPS_TO_MLO_RIP_RATIO = 1L; // lets all work in twip. there are lots of rounding errors if we don't. anyway twip is 1/20 of point and not 1/10.
// The ratio mentioned above

CG_INLINE MLODpx MLODpxByRip(MLORip rip)
{
    return (MLODpx) (((double)rip) / ((double)LO_TWIPS_TO_MLO_RIP_RATIO));
}

CG_INLINE MLORip MLORipByDpx(MLODpx dpx)
{
    return (MLORip) (((double)dpx) * ((double)LO_TWIPS_TO_MLO_RIP_RATIO));
}


#else
typedef basegfx::B2DRange MLODpxRect;
typedef float MLODpx;
typedef basegfx::B2DTuple MLODpxPoint;
typedef basegfx::B1DRange MLODpxSize;
#endif


// ========================
// Operating System generic
// ========================

void * mlo_string_from_enum_value(int enumValue, int * pEnumId, const char * allValues, const char * unkownValue);

int mlo_enum_from_enum_string_value(void * pVoidOfString, int defaultEnumValue, int * pEnumId, const char * allValues, const char * unkownValue);


static inline
MLORipSize MLORipSizeByDpxSize(MLODpxSize dpxSize)
{
    return MLORipSizeByRips(MLORipByDpx(dpxSize.width),
                            MLORipByDpx(dpxSize.height));
}

static inline
MLODpxSize MLODpxSizeByRips(MLORip ripWidth, MLORip ripHeight)
{
    return MLODpxSizeByDpxes(MLODpxByRip(ripWidth),
                             MLODpxByRip(ripHeight));
}

static inline
MLODpxSize MLODpxSizeByRipSize(MLORipSize ripSize)
{
    return MLODpxSizeByRips(ripSize.width, ripSize.height);
}

static inline
MLODpxSize MLODpxSizeByTwips(MLOTwip twipWidth, MLOTwip twipHeight)
{
    return MLODpxSizeByRipSize(MLORipSizeByTwips(twipWidth,
                                                 twipHeight));
}

static inline
MLORipPoint MLORipPointByDpxPoint(MLODpxPoint dpxPoint)
{
    return MLORipPointByRips(MLORipByDpx(dpxPoint.x),
                             MLORipByDpx(dpxPoint.y));
}

static inline
MLODpxPoint MLODpxPointByRipPoint(MLORipPoint ripPoint)
{
    return MLODpxPointByDpxes(MLODpxByRip(ripPoint.x),
                              MLODpxByRip(ripPoint.y));
}

static inline
MLODpxPoint MLODpxPointByTwips(MLOTwip twipX,MLOTwip twipY)
{
    return MLODpxPointByRipPoint(MLORipPointByTwips(twipX,
                                                    twipY));
}

static inline
MLORipRect MLORipRectByDpxRect(MLODpxRect dpxRect)
{
    return MLORipRectMake(MLORipPointByDpxPoint(dpxRect.origin),
                          MLORipSizeByDpxSize(dpxRect.size));
}

static inline
MLODpxRect MLODpxRectByDpxPointAndSize(MLODpxPoint dpxOrigin,MLODpxSize dpxSize)
{
    return MLODpxRectByDpxes(dpxOrigin.x,
                             dpxOrigin.y,
                             dpxSize.width,
                             dpxSize.height);
}

static inline
MLODpxRect MLODpxRectByRipPointAndSize(MLORipPoint ripOrigin, MLORipSize ripSize)
{
    return MLODpxRectByDpxPointAndSize(MLODpxPointByRipPoint(ripOrigin),
                                       MLODpxSizeByRipSize(ripSize));
}

static inline
MLODpxRect MLODpxRectByRips(MLORip ripX,MLORip ripY,MLORip ripWidth,MLORip ripHeight)
{
    return MLODpxRectByRipPointAndSize(MLORipPointByRips(ripX,
                                                         ripY),
                                       MLORipSizeByRips(ripWidth,
                                                        ripHeight));
}

static inline
MLODpxRect MLODpxRectByTwips(MLOTwip twipX,MLOTwip twipY, MLOTwip twipWidth, MLOTwip twipHeight)
{
    return MLODpxRectByRipPointAndSize(MLORipPointByTwips(twipX,
                                                          twipY),
                                       MLORipSizeByTwips(twipWidth,
                                                         twipHeight));
}

#ifdef __cplusplus
}
#endif

#endif // INCLUDED_TOUCH_TOUCH_SAL_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
