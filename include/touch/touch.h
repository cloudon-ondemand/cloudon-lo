/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INCLUDED_TOUCH_TOUCH_H
#define INCLUDED_TOUCH_TOUCH_H

#include <config_features.h>

#include <sys/time.h>


#ifdef IOS

#include <premac.h>
#include <CoreGraphics/CoreGraphics.h>
#include <postmac.h>

#endif


/*
double elapsed;

#define START_ELAPSED() timeval tv1, tv2; \
        elapsed = 0;\
	gettimeofday(&tv1, NULL);
	
#define END_ELAPSED() gettimeofday(&tv2, NULL); \
	elapsed = tv2.tv_sec-tv1.tv_sec + (tv2.tv_usec - tv1.tv_usec)/1e6;\
        SAL_DEBUG("File: "<<__FILE__<< " Line: "<< __LINE__ <<" Elapsed is "<< elapsed); 
*/
//#define RENDERING_DEBUG

#define MOBILE_MAX_ZOOM_IN 600
#define MOBILE_MAX_ZOOM_OUT 80
#define MOBILE_ZOOM_SCALE_MULTIPLIER 10000

#if !HAVE_FEATURE_DESKTOP

// Let's try this way: Use Quartz 2D types for iOS, and LO's basegfx
// types for others, when/if this API is used for others. But of
// course, it is quite likely that some degree of redesign is needed
// at such a stage anyway...

#include "touch/touch-sal.h"

#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif
#endif

// logging
// =======

typedef enum {
    TouchUiLogLevel_TRACE,
    TouchUiLogLevel_DEBUG,
    TouchUiLogLevel_INFO,
    TouchUiLogLevel_WARNING,
    TouchUiLogLevel_ERROR,
    TouchUiLogLevel_OFF
}TouchUiLogLevel;

#define TOUCH_UI_LOG_STREAM(LOG_LEVEL, STREAM)\
{\
    ::std::ostringstream log_stream; \
    log_stream << STREAM; \
    const char *message = log_stream.str().c_str(); \
    touch_ui_log(LOG_LEVEL, message);\
}

#define TOUCH_UI_LOG_STREAM_WHEN_TIME(LOG_LEVEL, STREAM, TIME)\
{\
    static int occ = 0;\
    ::std::ostringstream log_stream; \
    if(occ%TIME == 0)\
    {\
        log_stream << "File="<<__FILE__<<", Line="<<__LINE__<<", Time="<<occ<<", "<<STREAM; \
        const char *message = log_stream.str().c_str(); \
        touch_ui_log(LOG_LEVEL, message);\
    }\
    occ++;\
}

void touch_ui_log(TouchUiLogLevel level, const char * message);

// Used
// ====

void touch_lo_action_callback(void * anyCallbackData);
void touch_lo_keyboard_input(int unicodeCharacter, int code);

// Callbacks
// =========

// These functions are the interface between the upper GUI layers of a
// LibreOffice-based app on a touch platform app and the lower "core"
// layers, used in cases where the core parts need to call
// functionality in the upper parts or vice versa.
//
// Thus there are two classes of functions here:
//
// 1) Those to be implemented in the upper layer and called by the
// lower layer. Prefixed by touch_ui_. The same API is used on each
// such platform. There are called from low level LibreOffice
// code. Note that these are just declared here in a header for a
// "touch" module, the per-platform implementations are elsewhere.

void touch_ui_did_become_renderable_page(unsigned short pageNum);
void touch_ui_did_become_renderable();

void touch_ui_damaged(MLODpxRect* rects, unsigned long rectCount);

// Dialogs, work in progress, no clear plan yet what to do

void touch_ui_did_start_event_loop();
// informing the mobile level, it can begin using libreoffice

void touch_lo_doc_size_changed();
// asking the mobile layer to poll the doc size

void touch_ui_keyboard_special_charcter(int specialCharacter);
// allowing to handle special charcters by mobile implementation (e.g delete/backspace)
// Note:
// the callback is called from within the handling of keyboard event

void touch_ui_did_handle_keyboard_character(int character);
// called after each keyboard event, allowing to update the mobile views

long touch_ui_get_max_width();
// called so to be able to center page efficiently

void touch_ui_call_callback(void * anyCallbackData);
// allowing to call any callback from the event queue

void touch_ui_will_invoke_event();
// a callback called whenever invoking a touch event

void touch_ui_did_complete_event();
// a callback called whenever completing invocation of an event

typedef enum {
    TouchUiProgressType_UPDATE,
    TouchUiProgressType_MAX,
    TouchUiProgressType_RESET
}TouchUiProgressType;

#define TouchUiProgressType_UPDATE_UNKOWN_NODE_ORDER -1L
void touch_ui_update_progress(TouchUiProgressType type, long value);
// a callback used to track loading and saving progress

bool touch_ui_request_password(void * pOUStringPassword);
// a callback used to allow the device to provide password
// the pOUStringPassword needs to be casted to OUString * to be set
// if the method returns false, the load process is aborted

// 2) Those implemented in the lower layers to be called by the upper
// layer, in cases where we don't want to include a bunch of the
// "normal" LibreOffice C++ headers in an otherwise purely Objective-C
// CocoaTouch-based source file. Of course it depends on the case
// where that is wanted, and this all is work in progress. Prefixed by
// touch_lo_. All these are called on the UI thread and except for
// those so marked schedule work to be done asynchronously on the LO
// thread.

// Infra
// =====

void touch_lo_copy_buffer(const void * source, size_t sourceWidth, size_t sourceHeight, size_t sourceBytesPerRow, void * target, size_t targetWidth, size_t targetHeight);

// Bootstrapping
// =============
// Special case: This is the function that is called in the newly
// created LO thread to run the LO code.
void touch_lo_runMain();


// Deprecated
// ==========

void touch_lo_set_view_size(int width, int height);

// Special case: synchronous: waits for the rendering to complete
void touch_lo_render_windows(void *context, int minX, int minY, int width, int height);

typedef enum { DOWN, MOVE, UP} MLOMouseButtonState;
typedef enum { NO_MODIFIERS, SHIFT_MODIFIER, META_MODIFIER } MLOModifiers;
typedef int MLOModifierMask;

void touch_lo_tap(int x, int y);
void touch_lo_mouse(int x, int y, MLOMouseButtonState state, MLOModifierMask modifiers);
void touch_lo_pan(int deltaX, int deltaY);
void touch_lo_zoom(int x, int y, float scale);
void touch_lo_mouse_drag(int x, int y, MLOMouseButtonState state);

CGImageRef touch_reduce_image_resolution(CGImageRef image);

// Debug
// ======

#ifdef RENDERING_DEBUG

void touch_lo_write_context_to_image(CGContextRef context);

#endif

// Unused
// ======

void touch_lo_selection_attempt_resize(const void *documentHandle,
                                       MLODpxRect *selectedRectangles,
                                       int numberOfRectangles);

typedef enum {
    MLODialogMessage,
    MLODialogInformation,
    MLODialogWarning,
    MLODialogError,
    MLODialogQuery
} MLODialogKind;

typedef enum {
    MLODialogOK,
    MLODialogCancel,
    MLODialogNo,
    MLODialogYes,
    MLODialogRetry,
    MLODialogIgnore,
} MLODialogResult;

MLODialogResult touch_ui_dialog_modal(MLODialogKind kind, const char *message);

void touch_ui_show_keyboard();
void touch_ui_hide_keyboard();
bool touch_ui_keyboard_visible();
void touch_lo_keyboard_did_hide();

#ifdef __cplusplus
}
#endif

#endif // HAVE_FEATURE_DESKTOP

#endif // INCLUDED_TOUCH_TOUCH_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
