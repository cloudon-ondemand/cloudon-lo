/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#ifndef TOUCH_DUMP_HXX
#define TOUCH_DUMP_HXX

#include <sal/log.hxx>
#include <vector>
#include <stdarg.h>

#define TOUCH_DUMP_AREA "sw.core"

#define xmlNewTextWriterFilename(FILENAME, COMPRESSION) (xmlTextWriterPtr)new TouchDumpWriter(); SAL_INFO(TOUCH_DUMP_AREA, "STARTING DUMP "<<FILENAME);

#define xmlFreeTextWriter(WRITER) TouchDumpWriter* touchDumpWriter=(TouchDumpWriter*)WRITER; delete touchDumpWriter;

#define xmlTextWriterStartDocument(WRITER, VERSION, ENCODING, STANDALONE) touchDumpWriterStartDocument((TouchDumpWriter*)WRITER, (const char*)VERSION, (const char*)ENCODING, (const char*)STANDALONE);

#define xmlTextWriterEndDocument(WRITER) touchDumpWriterEndDocument((TouchDumpWriter*)WRITER);

#define xmlTextWriterStartElement(WRITER, ELEMENT) touchDumpWriterStartElement((TouchDumpWriter*)WRITER,(const char*)ELEMENT);

#define xmlTextWriterEndElement(WRITER) touchDumpWriterEndElement((TouchDumpWriter*)WRITER);

#define xmlTextWriterWriteFormatAttribute(WRITER, NAME, FORMAT, __VA_ARGS__) touchDumpWriterFormatAttribute((TouchDumpWriter*)WRITER, (const char*)NAME, (const char*)FORMAT, __VA_ARGS__);

#define xmlTextWriterWriteVFormatAttribute(WRITER, NAME, FORMAT, ARGPTR) touchDumpWriterVFormatAttribute((TouchDumpWriter*)WRITER, (const char*)NAME, (const char*)FORMAT, ARGPTR);

#define xmlTextWriterWriteString(WRITER, CONTENT) touchDumpWriterWriteString((TouchDumpWriter*)(xmlTextWriterPtr)WRITER, (const char*)CONTENT);

class TouchDumpWriter
{
    private:

        std::vector<const char*> maElements;

    public:

        TouchDumpWriter();

        TouchDumpWriter(TouchDumpWriter* writer);

        virtual ~TouchDumpWriter() {}

        short getElementsCount(){return (char)maElements.size();}

        void startElement(const char *elementName);

        void endElement();

        void writeFormatAttribute( const char* attribute, const char* format, ... );

        void writeVFormatAttribute( const char* attribute, const char* format, va_list argptr );
};

void touchDumpWriterStartDocument(TouchDumpWriter *touchDumpWriter, const char * version, const char* encoding, const char* standalone);

void touchDumpWriterEndDocument(TouchDumpWriter *touchDumpWriter);

void touchDumpWriterStartElement(TouchDumpWriter *touchDumpWriter, const char * elementName);

void touchDumpWriterEndElement(TouchDumpWriter *touchDumpWriter);

void touchDumpWriterFormatAttribute(TouchDumpWriter *touchDumpWriter, const char *name, const char *format, ...);

void touchDumpWriterVFormatAttribute(TouchDumpWriter *touchDumpWriter, const char *name, const char *format, va_list argptr);

void touchDumpWriterWriteString(TouchDumpWriter *touchDumpWriter, const char *content);

#endif
