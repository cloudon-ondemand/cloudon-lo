//
//  touch-spelling.h
//  MobileLibreOffice
//
//  Created by ptyl on 7/14/14.
//  Copyright (c) 2014 LibreOffice.org. All rights reserved.
//

#ifndef INCLUDED_TOUCH_TOUCH_SPELL_H
#define INCLUDED_TOUCH_TOUCH_SPELL_H

#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif
#endif

void touch_ui_populate_locales(void * pVoidToVectorOfLocales);

void touch_ui_populate_spelling_proposals(const void * pVoidToOUString,
                                          const void * pVoidToLocale,
                                          void * pVoidToVectorOfOUString);

bool touch_ui_has_spelling_error(const void * pVoidToOUString,
                                 const void * pVoidToLocale);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDED_TOUCH_TOUCH_SPELL_H */
