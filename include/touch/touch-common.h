/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * Copyright 2013 LibreOffice contributors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INCLUDED_TOUCH_TOUCH_COMMON_H
#define INCLUDED_TOUCH_TOUCH_COMMON_H

#ifdef __cplusplus
extern "C" {
#if 0
} // To avoid an editor indenting all inside the extern "C"
#endif
#endif

typedef float MLOTwip;
//graphic units
typedef float MLORip;
// RIP - short for Ratio of an Inch Point

static inline MLORip MLORipByTwip(MLOTwip twip)
{
    return (MLORip) twip;
}

static inline MLOTwip MLOTwipByRip(MLORip rip)
{
    return (MLOTwip) rip;
}

// RIP SIZE

typedef struct
{
    MLORip width;
    MLORip height;
}
MLORipSize;

static inline MLORipSize MLORipSizeByRips(MLORip width, MLORip height)
{
    MLORipSize ripSize;
    ripSize.width = width;
    ripSize.height = height;
    return ripSize;
}

static inline MLORipSize MLORipSizeByTwips(MLOTwip width, MLOTwip height)
{
    return MLORipSizeByRips(MLORipByTwip(width),
                            MLORipByTwip(height));
}

// RIP POINT

typedef struct
{
    MLORip x;
    MLORip y;
}
MLORipPoint;



static inline MLORipPoint MLORipPointByRips(MLORip x, MLORip y)
{
    MLORipPoint point;
    point.x = x;
    point.y = y;
    return point;
}

static inline MLORipPoint MLORipPointByTwips(MLOTwip x, MLOTwip y)
{
    return MLORipPointByRips(MLORipByTwip(x),
                             MLORipByTwip(y));
}

// RIP RECT

typedef struct
{
    MLORipPoint origin;
    MLORipSize size;
}
MLORipRect;


static inline MLORipRect MLORipRectMake(MLORipPoint ripPoint,MLORipSize ripSize)
{
    MLORipRect ripRect;
    ripRect.origin = ripPoint;
    ripRect.size = ripSize;
    return ripRect;
}

static inline MLORipRect MLORipRectByRips(MLORip x, MLORip y,MLORip width,MLORip height)
{
    return MLORipRectMake(MLORipPointByRips(x, y),
                          MLORipSizeByRips(width,height));
}


static inline MLORipRect MLORipRectByTwips(MLOTwip x, MLOTwip y,MLOTwip width,MLOTwip height)
{
    return MLORipRectByRips(MLORipByTwip(x),
                            MLORipByTwip(y),
                            MLORipByTwip(width),
                            MLORipByTwip(height));
}

#define MLORipSizeZero MLORipSizeByRips(0,0)
#define MLORipPointZero MLORipPointByRips(0,0)
#define MLORipRectZero MLORipRectByRips(0,0,0,0)

#ifdef __cplusplus
}
#endif

#endif // INCLUDED_TOUCH_TOUCH_COMMON_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
