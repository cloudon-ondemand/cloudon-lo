#!/bin/bash

cwd=$(pwd)

echo $cwd

if [ $# -ge 2 ]
then
	buildDir="$1"
	iosDir="$2"
else
	echo ".makeAndCopy <buildDir> <iosDir>"	
	exit 7
fi

relLibsPath="ios/shared_dep/libs/"
buildDirLibsPath=$buildDir$relLibsPath

relIosLibsPath="CloudOn-CommonLayer/CAT/engine/libs/"
iosDirLibsPath=$iosDir$relIosLibsPath

echo "Making $buildDir and then synching $buildDirLibsPath with $iosDirLibsPath"

#lastTime=$(find ./ios/shared_dep/ -exec stat -f "%m" \{} \; | sort -n -r | head -1)

cd $buildDir

make $3 2>&1 | tee $cwd/build.log

cd $cwd

#rm -rf ~/workspace/ios/CloudOn-CommonLayer/CAT/engine/*
#cp -r ios/shared_dep/* ~/workspace/ios/CloudOn-CommonLayer/CAT/engine

#rsync -rav --copy-links ios/shared_dep/libs/ ~/workspace/ios/CloudOn-CommonLayer/CAT/engine/libs/ 2>&1 | tee rsync.log

rsync -rav --copy-links $buildDirLibsPath $iosDirLibsPath 2>&1 | tee rsync.log

date
