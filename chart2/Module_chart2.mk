# -*- Mode: makefile-gmake; tab-width: 4; indent-tabs-mode: t -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

ifeq ($(OS),IOS)
ifneq (,$(findstring chart2, $(SYMBOL_LIBS)))
tmp_gb_LinkTarget_CFLAGS := $(gb_LinkTarget_CFLAGS)
tmp_gb_LinkTarget_CXXFLAGS := $(gb_LinkTarget_CXXFLAGS)
tmp_gb_LinkTarget_OBJCXXFLAGS := $(gb_LinkTarget_OBJCXXFLAGS)
tmp_gb_LinkTarget_OBJCFLAGS := $(gb_LinkTarget_OBJCFLAGS)
gb_LinkTarget_CFLAGS += -g
gb_LinkTarget_CXXFLAGS += -g
gb_LinkTarget_OBJCXXFLAGS += -g
gb_LinkTarget_OBJCFLAGS += -g
endif
endif

$(eval $(call gb_Module_Module,chart2))

$(eval $(call gb_Module_add_targets,chart2,\
    Library_chartcontroller \
    Library_chartcore \
    Library_chartopengl \
    Package_opengl \
))

$(eval $(call gb_Module_add_l10n_targets,chart2,\
    AllLangResTarget_chartcontroller \
	UIConfig_chart2 \
))

$(eval $(call gb_Module_add_slowcheck_targets,chart2,\
    CppunitTest_chart2_export \
    CppunitTest_chart2_import \
))

ifeq ($(ENABLE_CHART_TESTS),TRUE)
$(eval $(call gb_Module_add_slowcheck_targets,chart2,\
    CppunitTest_chart2_xshape \
))

endif

$(eval $(call gb_Module_add_subsequentcheck_targets,chart2,\
    JunitTest_chart2_unoapi \
))

ifeq ($(OS),IOS)
ifneq (,$(findstring chart2, $(SYMBOL_LIBS)))
gb_LinkTarget_CFLAGS := $(tmp_gb_LinkTarget_CFLAGS)
gb_LinkTarget_CXXFLAGS := $(tmp_gb_LinkTarget_CXXFLAGS)
gb_LinkTarget_OBJCXXFLAGS := $(tmp_gb_LinkTarget_OBJCXXFLAGS)
gb_LinkTarget_OBJCFLAGS := $(tmp_gb_LinkTarget_OBJCFLAGS)
endif
endif

# vim: set noet sw=4 ts=4:
