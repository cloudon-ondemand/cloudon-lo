#!/bin/bash

cwd=$(pwd)

echo $cwd

if [ $# -ge 2 ]
then
    buildDir="$1"
    iosDir="$2"
else
    echo ".copy <buildDir> <iosDir>"
    exit 7
fi

relLibsPath="ios/shared_dep/libs/"
buildDirLibsPath=$buildDir$relLibsPath

relIosLibsPath="CloudOn-CommonLayer/CAT/engine/libs/"
iosDirLibsPath=$iosDir$relIosLibsPath

echo "synching $buildDirLibsPath with $iosDirLibsPath"

cd $cwd

rsync -rav --copy-links $buildDirLibsPath $iosDirLibsPath 2>&1 | tee rsync.log

date
