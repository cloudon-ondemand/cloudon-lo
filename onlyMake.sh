#!/bin/bash

cwd=$(pwd)

echo $cwd

if [ $# -ge 1 ]
then
    buildDir="$1"
else
    echo ".makeAndCopy <buildDir> "
    exit 7
fi

relLibsPath="ios/shared_dep/libs/"
buildDirLibsPath=$buildDir$relLibsPath

echo "Making $buildDir and then synching $buildDirLibsPath with $iosDirLibsPath"

cd $buildDir

make $2 2>&1 | tee build.log

cd $cwd

date
