/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <com/sun/star/uno/Reference.h>
#include <com/sun/star/linguistic2/XSearchableDictionaryList.hpp>

#include <com/sun/star/linguistic2/SpellFailure.hpp>
#include <cppuhelper/factory.hxx>
#include <cppuhelper/supportsservice.hxx>
#include <com/sun/star/registry/XRegistryKey.hpp>
#include <tools/debug.hxx>
#include <osl/mutex.hxx>

#include <iosspellimp.hxx>
#include <touch/touch-spell.h>

#include <linguistic/spelldta.hxx>
#include <unotools/pathoptions.hxx>
#include <unotools/useroptions.hxx>
#include <osl/file.hxx>
#include <rtl/ustrbuf.hxx>

using namespace std;
using namespace utl;
using namespace osl;
using namespace com::sun::star;
using namespace com::sun::star::beans;
using namespace com::sun::star::lang;
using namespace com::sun::star::uno;
using namespace com::sun::star::linguistic2;
using namespace linguistic;

using ::rtl::OUString;
using ::rtl::OString;
using ::rtl::OUStringBuffer;
using ::rtl::OUStringToOString;

static
sal_Bool lcl_isEqualLocale(const Locale & l1,
                           const Locale & l2)
{
    return (l1.Language == l2.Language) &&
    (l1.Country == l2.Country) &&
    (l1.Variant == l2.Variant);
}

IOSSpellChecker::IOSSpellChecker() :
aEvtListeners( GetLinguMutex() )
{
    aDEncs = NULL;
    aDLocs = NULL;
    aDNames = NULL;
    bDisposing = false;
    pPropHelper = NULL;
    numdict = 0;
}



IOSSpellChecker::~IOSSpellChecker()
{
    numdict = 0;
    if (aDEncs) delete[] aDEncs;
    aDEncs = NULL;
    if (aDLocs) delete[] aDLocs;
    aDLocs = NULL;
    if (aDNames) delete[] aDNames;
    aDNames = NULL;
    if (pPropHelper)
        pPropHelper->RemoveAsPropListener();
}


PropertyHelper_Spell & IOSSpellChecker::GetPropHelper_Impl()
{
    if (!pPropHelper)
    {
        Reference< XLinguProperties >   xPropSet( GetLinguProperties(), UNO_QUERY );

        pPropHelper = new PropertyHelper_Spell( (XSpellChecker *) this, xPropSet );
        xPropHelper = pPropHelper;
        pPropHelper->AddAsPropListener();   //! after a reference is established
    }
    return *pPropHelper;
}


Sequence< Locale > SAL_CALL IOSSpellChecker::getLocales()
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    // this routine should return the locales supported by the installed
    // dictionaries.  So here we need to parse both the user edited
    // dictionary list and the shared dictionary list
    // to see what dictionaries the admin/user has installed

    if (!numdict)
    {
        vector<Locale> locales;
        touch_ui_populate_locales(reinterpret_cast<void *>(&locales));

        // we really should merge these and remove duplicates but since
        // users can name their dictionaries anything they want it would
        // be impossible to know if a real duplication exists unless we
        // add some unique key to each myspell dictionary
        numdict = locales.size();

        if (numdict) {
            aDLocs = new Locale [numdict];
            aDEncs  = new rtl_TextEncoding [numdict];
            aDNames = new OUString [numdict];
            aSuppLocales.realloc(numdict);
            Locale * pLocale = aSuppLocales.getArray();
            int numlocs = 0;
            int newloc;
            int k = 0;

            // now add the shared dictionaries
            for (auto & currentLocale : locales)
            {
                newloc = 1;
                //eliminate duplicates (is this needed for MacOS?)
                for (int j = 0; j < numlocs; j++)
                {
                    if (lcl_isEqualLocale(currentLocale,
                                            pLocale[j]))
                    {
                        newloc = 0;
                    }
                }
                if (newloc)
                {
                    pLocale[numlocs] = currentLocale;
                    numlocs++;
                }
                aDLocs[k] = currentLocale;
                aDEncs[k] = 0;
                k++;
            }

            aSuppLocales.realloc(numlocs);

        } else {
            /* no dictionary.lst found so register no dictionaries */
            numdict = 0;
            aDEncs  = nullptr;
            aDLocs = nullptr;
            aDNames = nullptr;
            aSuppLocales.realloc(0);
        }
    }

    return aSuppLocales;
}


sal_Bool SAL_CALL IOSSpellChecker::hasLocale(const Locale& rLocale)
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    sal_Bool bRes = sal_False;
    if (!aSuppLocales.getLength())
        getLocales();

    sal_Int32 nLen = aSuppLocales.getLength();
    for (sal_Int32 i = 0;  i < nLen;  ++i)
    {
        const Locale *pLocale = aSuppLocales.getConstArray();
        if (lcl_isEqualLocale(rLocale,
                              pLocale[i]))
        {
            bRes = sal_True;
            break;
        }
    }
    return bRes;
}


sal_Int16 IOSSpellChecker::GetSpellFailure( const OUString &rWord, const Locale &rLocale )
{
    sal_Int16 nRes = -1;

    // first handle smart quotes both single and double
    OUStringBuffer rBuf(rWord);
    sal_Int32 n = rBuf.getLength();
    sal_Unicode c;
    for (sal_Int32 ix=0; ix < n; ix++) {
        c = rBuf[ix];
        if ((c == 0x201C) ||
            (c == 0x201D))
        {
            rBuf[ix] = (sal_Unicode)0x0022;
        }
        if ((c == 0x2018) ||
            (c == 0x2019))
        {
            rBuf[ix] = (sal_Unicode)0x0027;
        }
    }
    OUString nWord(rBuf.makeStringAndClear());

    if (n)
    {
        int rVal = 0;
        if(touch_ui_has_spelling_error(reinterpret_cast<const void *>(&nWord),
                                       reinterpret_cast<const void *>(&rLocale)))
        {
            rVal = -1;
        }
        else
        {
            rVal = 1;
        }
        if (rVal != 1)
        {
            nRes = SpellFailure::SPELLING_ERROR;
        } else {
            return -1;
        }
    }
    return nRes;
}



sal_Bool SAL_CALL
    IOSSpellChecker::isValid( const OUString& rWord, const Locale& rLocale,
            const PropertyValues& rProperties )
        throw(IllegalArgumentException, RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (lcl_isEqualLocale(rLocale,
                          Locale())  ||
        !rWord.getLength())
    {
        return sal_True;
    }
    if (!hasLocale( rLocale ))
    {
        return sal_True;
    }
    // Get property values to be used.
    // These are be the default values set in the SN_LINGU_PROPERTIES
    // PropertySet which are overridden by the supplied ones from the
    // last argument.
    // You'll probably like to use a simpler solution than the provided
    // one using the PropertyHelper_Spell.

    PropertyHelper_Spell &rHelper = GetPropHelper();
    rHelper.SetTmpPropVals( rProperties );

    sal_Int16 nFailure = GetSpellFailure( rWord, rLocale );
    if (nFailure != -1)
    {
        sal_Int16 nLang = LinguLocaleToLanguage( rLocale );
        // postprocess result for errors that should be ignored
        if (   (!rHelper.IsSpellUpperCase()  && IsUpper( rWord, nLang ))
            || (!rHelper.IsSpellWithDigits() && HasDigits( rWord ))
            || (!rHelper.IsSpellCapitalization()
                &&  nFailure == SpellFailure::CAPTION_ERROR)
            )
            nFailure = -1;
    }

    return (nFailure == -1);
}


Reference< XSpellAlternatives >
    IOSSpellChecker::GetProposals( const OUString &rWord, const Locale &rLocale )
{
    // Retrieves the return values for the 'spell' function call in case
    // of a misspelled word.
    // Especially it may give a list of suggested (correct) words:

    Reference< XSpellAlternatives > xRes;
    // note: mutex is held by higher up by spell which covers both

    sal_Int16 nLang = LinguLocaleToLanguage( rLocale );
    Sequence< OUString > aStr( 0 );

    // first handle smart quotes (single and double)
    OUStringBuffer rBuf(rWord);
    sal_Int32 n = rBuf.getLength();
    sal_Unicode c;
    for (sal_Int32 ix=0; ix < n; ix++) {
        c = rBuf[ix];
        if ((c == 0x201C) || (c == 0x201D)) rBuf[ix] = (sal_Unicode)0x0022;
        if ((c == 0x2018) || (c == 0x2019)) rBuf[ix] = (sal_Unicode)0x0027;
    }
    OUString nWord(rBuf.makeStringAndClear());

    if (n)
    {
        vector<OUString> vectorOfOUStrings;
        touch_ui_populate_spelling_proposals(reinterpret_cast<const void*>(&nWord),
                                             reinterpret_cast<const void*>(&rLocale),
                                             reinterpret_cast<void*>(&vectorOfOUStrings));
        int count = vectorOfOUStrings.size();
        if (count)
        {
            aStr.realloc( count );
            OUString *pStr = aStr.getArray();
            for (int ii=0; ii < count; ii++)
            {
                // if needed add: if (suglst[ii] == NULL) continue;
                pStr[ii] = vectorOfOUStrings[ii];
            }
        };
    }

    // now return an empty alternative for no suggestions or the list of alternatives if some found
    SpellAlternatives *pAlt = new SpellAlternatives;
    pAlt->SetWordLanguage( rWord, nLang );
    pAlt->SetFailureType( SpellFailure::SPELLING_ERROR );
    pAlt->SetAlternatives( aStr );
    xRes = pAlt;
    return xRes;

}




Reference< XSpellAlternatives > SAL_CALL
    IOSSpellChecker::spell( const OUString& rWord, const Locale& rLocale,
            const PropertyValues& rProperties )
        throw(IllegalArgumentException, RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (lcl_isEqualLocale(rLocale, {})  ||
        !rWord.getLength())
    {
        return NULL;
    }
    if (!hasLocale( rLocale ))
    {
        return NULL;
    }

    Reference< XSpellAlternatives > xAlt;
    if (!isValid( rWord, rLocale, rProperties ))
    {
        xAlt =  GetProposals( rWord, rLocale );
    }
    return xAlt;
}


Reference< XInterface > SAL_CALL IOSSpellChecker_CreateInstance(
            const Reference< XMultiServiceFactory > & /*rSMgr*/ )
        throw(Exception)
{

    Reference< XInterface > xService = (cppu::OWeakObject*) new IOSSpellChecker;
    return xService;
}


sal_Bool SAL_CALL
    IOSSpellChecker::addLinguServiceEventListener(
            const Reference< XLinguServiceEventListener >& rxLstnr )
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    sal_Bool bRes = sal_False;
    if (!bDisposing && rxLstnr.is())
    {
        bRes = GetPropHelper().addLinguServiceEventListener( rxLstnr );
    }
    return bRes;
}


sal_Bool SAL_CALL
    IOSSpellChecker::removeLinguServiceEventListener(
            const Reference< XLinguServiceEventListener >& rxLstnr )
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    sal_Bool bRes = sal_False;
    if (!bDisposing && rxLstnr.is())
    {
        DBG_ASSERT( xPropHelper.is(), "xPropHelper non existent" );
        bRes = GetPropHelper().removeLinguServiceEventListener( rxLstnr );
    }
    return bRes;
}


OUString SAL_CALL
    IOSSpellChecker::getServiceDisplayName( const Locale& /*rLocale*/ )
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );
    return OUString( "iOS Spell Checker" );
}


void SAL_CALL
    IOSSpellChecker::initialize( const Sequence< Any >& rArguments )
        throw(Exception, RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (!pPropHelper)
    {
        sal_Int32 nLen = rArguments.getLength();
        if (2 == nLen)
        {
            Reference< XLinguProperties >   xPropSet;
            rArguments.getConstArray()[0] >>= xPropSet;
            //rArguments.getConstArray()[1] >>= xDicList;

            //! Pointer allows for access of the non-UNO functions.
            //! And the reference to the UNO-functions while increasing
            //! the ref-count and will implicitly free the memory
            //! when the object is not longer used.
            pPropHelper = new PropertyHelper_Spell( (XSpellChecker *) this, xPropSet );
            xPropHelper = pPropHelper;
            pPropHelper->AddAsPropListener();   //! after a reference is established
        }
        else
            OSL_FAIL( "wrong number of arguments in sequence" );

    }
}


void SAL_CALL
    IOSSpellChecker::dispose()
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (!bDisposing)
    {
        bDisposing = true;
        EventObject aEvtObj( (XSpellChecker *) this );
        aEvtListeners.disposeAndClear( aEvtObj );
    }
}


void SAL_CALL
    IOSSpellChecker::addEventListener( const Reference< XEventListener >& rxListener )
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (!bDisposing && rxListener.is())
        aEvtListeners.addInterface( rxListener );
}


void SAL_CALL
    IOSSpellChecker::removeEventListener( const Reference< XEventListener >& rxListener )
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    if (!bDisposing && rxListener.is())
        aEvtListeners.removeInterface( rxListener );
}

// Service specific part
OUString SAL_CALL IOSSpellChecker::getImplementationName()
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    return getImplementationName_Static();
}

sal_Bool SAL_CALL IOSSpellChecker::supportsService( const OUString& ServiceName )
        throw(RuntimeException)
{
    return cppu::supportsService(this, ServiceName);
}

Sequence< OUString > SAL_CALL IOSSpellChecker::getSupportedServiceNames()
        throw(RuntimeException)
{
    MutexGuard  aGuard( GetLinguMutex() );

    return getSupportedServiceNames_Static();
}

Sequence< OUString > IOSSpellChecker::getSupportedServiceNames_Static()
        throw()
{
    MutexGuard  aGuard( GetLinguMutex() );

    Sequence< OUString > aSNS( 1 ); // auch mehr als 1 Service moeglich
    aSNS.getArray()[0] = SN_SPELLCHECKER;
    return aSNS;
}

void * SAL_CALL IOSSpellChecker_getFactory( const sal_Char * pImplName,
            XMultiServiceFactory * pServiceManager, void *  )
{
    void * pRet = 0;
    if ( IOSSpellChecker::getImplementationName_Static().equalsAscii( pImplName ) )
    {
        Reference< XSingleServiceFactory > xFactory =
            cppu::createOneInstanceFactory(
                pServiceManager,
                IOSSpellChecker::getImplementationName_Static(),
                IOSSpellChecker_CreateInstance,
                IOSSpellChecker::getSupportedServiceNames_Static());
        // acquire, because we return an interface pointer instead of a reference
        xFactory->acquire();
        pRet = xFactory.get();
    }
    return pRet;
}




/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
