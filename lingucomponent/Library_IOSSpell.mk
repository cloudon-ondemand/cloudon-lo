# -*- Mode: makefile-gmake; tab-width: 4; indent-tabs-mode: t -*-
#
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

$(eval $(call gb_Library_Library,IOSSpell))

$(eval $(call gb_Library_set_componentfile,IOSSpell,lingucomponent/source/spellcheck/iosspell/IOSSpell))

$(eval $(call gb_Library_set_include,IOSSpell,\
	$$(INCLUDE) \
	-I$(SRCDIR)/lingucomponent/source/lingutil \
))

$(eval $(call gb_Library_use_sdk_api,IOSSpell))

$(eval $(call gb_Library_use_libraries,IOSSpell,\
	cppu \
	cppuhelper \
	lng \
	sal \
	i18nlangtag \
	svl \
	tl \
	ucbhelper \
	utl \
))

$(eval $(call gb_Library_use_system_darwin_frameworks,IOSSpell,\
	Cocoa \
))

$(eval $(call gb_Library_use_externals,IOSSpell,\
	boost_headers \
	hunspell \
))

$(eval $(call gb_Library_add_exception_objects,IOSSpell,\
	lingucomponent/source/spellcheck/iosspell/iosreg \
	lingucomponent/source/spellcheck/iosspell/iosspellimp \
))

# vim: set noet sw=4 ts=4:
