/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <touch/touch-dump.hxx>
#include <stdio.h>

#define TOUCH_DUMP(STRING, ELEMENT_COUNT)\
{\
    ::std::ostringstream tab_stream; \
    for(short tabNum = 0; tabNum<ELEMENT_COUNT; tabNum++){\
        tab_stream<<"\t";\
    }\
    SAL_INFO(TOUCH_DUMP_AREA, tab_stream.str()<<STRING);\
}

TouchDumpWriter::TouchDumpWriter():maElements()
{
}

TouchDumpWriter::TouchDumpWriter(TouchDumpWriter* writer):maElements(writer->maElements)
{
}

void TouchDumpWriter::startElement(const char *elementName)
{
    TOUCH_DUMP(elementName<<" - START" << ", ElementCount="<<getElementsCount(), getElementsCount());

    std::vector<const char*>::iterator it = maElements.begin();

    maElements.insert(it, elementName);
}

void TouchDumpWriter::endElement()
{
    const char* lastElement = *(maElements.begin());

    maElements.erase(maElements.begin());

    TOUCH_DUMP(lastElement<<" - END, ElementCount="<<getElementsCount(), getElementsCount());
}

void TouchDumpWriter::writeFormatAttribute( const char* attribute, const char* format, ... )
{
    va_list ap;

    va_start(ap, format);

    writeVFormatAttribute(attribute, format, ap);

    va_end(ap);
}

void TouchDumpWriter::writeVFormatAttribute( const char* attribute, const char* format, va_list argptr )
{
    char value[256];

    vsprintf (value,(const char*)format, argptr);

    TOUCH_DUMP("Name="<<attribute<<", value="<<value, getElementsCount());
}

void touchDumpWriterStartDocument(TouchDumpWriter *touchDumpWriter, const char * version, const char* encoding, const char* standalone)
{
    (void)touchDumpWriter;
    (void)version;
    (void)encoding;
    (void)standalone;
}

void touchDumpWriterEndDocument(TouchDumpWriter *touchDumpWriter)
{
    (void)touchDumpWriter;
}

void touchDumpWriterStartElement(TouchDumpWriter *touchDumpWriter, const char * elementName)
{
    touchDumpWriter->startElement(elementName);
}

void touchDumpWriterEndElement(TouchDumpWriter *touchDumpWriter)
{
    touchDumpWriter->endElement();
}

void touchDumpWriterFormatAttribute(TouchDumpWriter *touchDumpWriter, const char *name, const char *format, ...)
{
    va_list ap;

    va_start(ap, format);

    touchDumpWriter->writeVFormatAttribute(name, format, ap);

    va_end(ap);
}

void touchDumpWriterVFormatAttribute(TouchDumpWriter *touchDumpWriter, const char *name, const char *format, va_list argptr)
{
    touchDumpWriter->writeVFormatAttribute(name, format, argptr);
}

void touchDumpWriterWriteString(TouchDumpWriter *touchDumpWriter, const char *content)
{
    (void)touchDumpWriter;

    TOUCH_DUMP(content, touchDumpWriter->getElementsCount());
}
