# -*- Mode: makefile-gmake; tab-width: 4; indent-tabs-mode: t -*-
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
ifeq ($(OS),IOS)
ifneq (,$(findstring basegfx, $(SYMBOL_LIBS)))
tmp_gb_LinkTarget_CFLAGS := $(gb_LinkTarget_CFLAGS)
tmp_gb_LinkTarget_CXXFLAGS := $(gb_LinkTarget_CXXFLAGS)
tmp_gb_LinkTarget_OBJCXXFLAGS := $(gb_LinkTarget_OBJCXXFLAGS)
tmp_gb_LinkTarget_OBJCFLAGS := $(gb_LinkTarget_OBJCFLAGS)
gb_LinkTarget_CFLAGS += -g
gb_LinkTarget_CXXFLAGS += -g
gb_LinkTarget_OBJCXXFLAGS += -g
gb_LinkTarget_OBJCFLAGS += -g
endif
endif

$(eval $(call gb_Module_Module,basegfx))

$(eval $(call gb_Module_add_targets,basegfx,\
    Library_basegfx \
))

$(eval $(call gb_Module_add_check_targets,basegfx,\
    CppunitTest_basegfx \
))

ifeq ($(OS),IOS)
ifneq (,$(findstring ucbhelper, $(SYMBOL_LIBS)))
gb_LinkTarget_CFLAGS := $(tmp_gb_LinkTarget_CFLAGS)
gb_LinkTarget_CXXFLAGS := $(tmp_gb_LinkTarget_CXXFLAGS)
gb_LinkTarget_OBJCXXFLAGS := $(tmp_gb_LinkTarget_OBJCXXFLAGS)
gb_LinkTarget_OBJCFLAGS := $(tmp_gb_LinkTarget_OBJCFLAGS)
endif
endif

# vim: set noet sw=4 ts=4:
